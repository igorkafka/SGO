﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SGO.Data;
using SGO.Interfaces;
using SGO.Models;

namespace SGO.Controllers
{
    [ApiController]
    public class MedicoController : ControllerBase
    {
        IRepositoryMedico _repositoryMedico;
        public MedicoController(IRepositoryMedico repositoryMedico)
        {
            _repositoryMedico = repositoryMedico;
        }
        [HttpPost]
        [Route("api/[controller]/create")]
        [Authorize("Bearer")]
        public void Create(Medico medico)
        {
           _repositoryMedico.Create(medico);
        }
        [HttpGet]
        [Route("api/[controller]/listarpornome")]
        [Authorize("Bearer")]
        public ICollection<Medico> ListarPorNome(string nome, int pagina)
        {
           return _repositoryMedico.ListarPorNome(nome, pagina);
        }
        [HttpGet]
        [Route("api/[controller]/listarporcrm")]
        [Authorize("Bearer")]
        public ICollection<Medico> ListarPorCrm(string crm, int pagina)
        {
            return _repositoryMedico.ListarPorCrm(crm, pagina);
        }
        [HttpGet]
        [Route("api/[controller]/listarporespecialidade")]
        [Authorize("Bearer")]
        public ICollection<Medico> ListarPorEspecialidade(string especialidade, int pagina)
        {
            return _repositoryMedico.ListarPorEspecialidade(especialidade, pagina);
        }
        [HttpGet]
        [Route("api/[controller]/listarpordatanascimento")]
        [Authorize("Bearer")]
        public ICollection<Medico> ListarPorDataNascimento(string data, int pagina)
        {
            return _repositoryMedico.ListarPorData(data, pagina);
        }
        [HttpPut]
        [Route("api/[controller]/atualizar")]
        [Authorize("Bearer")]
        public void Update(int id,[FromBody]Medico medico)
        {
            _repositoryMedico.Update(id, medico);
        }
    }
}