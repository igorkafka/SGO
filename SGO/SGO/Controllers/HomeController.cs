﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace SGO.Controllers
{
    public class HomeController : Controller
    {
        private IHostingEnvironment _env;
        public HomeController(IHostingEnvironment env)
        {

        }
        public IActionResult Index()
        {
            var webRoot = _env.WebRootPath;
            var path = System.IO.Path.Combine(webRoot, "index.html");
            return File(path, "text/html");
        }
    }
}