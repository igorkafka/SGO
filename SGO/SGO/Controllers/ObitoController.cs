﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SGO.Data;
using SGO.Interfaces;
using SGO.Models;

namespace SGO.Controllers
{
    [ApiController]
    public class ObitoController : ControllerBase
    {
        IRepositoryObito _repositoryObito;
        public ObitoController(IRepositoryObito repositoryObito)
        {
            _repositoryObito = repositoryObito;
        }
        [Authorize("Bearer")]
        [HttpPost]
        [Route("api/[controller]/create")]
        public void Create(Obito obito)
        {
            _repositoryObito.Create(obito);
        }
        [Authorize("Bearer")]
        [HttpPut]
        [Route("api/[controller]/update")]
        public void Update(Obito obito)
        {
            _repositoryObito.Update(obito.NumeroOrdem, obito);
        }
        [Authorize("Bearer")]
        [HttpGet]
        [Route("api/[controller]/listarpelanumeracao")]
        public ICollection<Obito> ListarPelaNumeracao(string numeracao, int pagina)
        {
            return _repositoryObito.ListarPelaNumeracao(numeracao, pagina);
        }
        [Authorize("Bearer")]
        [HttpGet]
        [Route("api/[controller]/listarpelonumerocaixa")]
        public ICollection<Obito> ListaPorCaixa(string caixa, int pagina)
        {
            return _repositoryObito.ListaPorCaixa(caixa, pagina);
        }
        [Authorize("Bearer")]
        [HttpGet]
        [Route("api/[controller]/listarpornumeroregistro")]
        public ICollection<Obito> ListarPorNumeroRegistro(string numeroregistro, int pagina)
        {
            return _repositoryObito.ListarPorNumeroRegistro(numeroregistro, pagina);
        }
        [Authorize("Bearer")]
        [Route("api/[controller]/listarpornomemedico")]
        public ICollection<Obito> ListarPorNomeDoMedico(string nomemedico, int pagina)
        {
            return _repositoryObito.ListarPorNomeDoMedico(nomemedico, pagina);
        }
        [Authorize("Bearer")]
        [Route("api/[controller]/listarpornomeclinica")]
        public ICollection<Obito> ListarPorNomeClinica(string nomeclinica, int pagina)
        {
            return _repositoryObito.ListarPorNomeDaClinica(nomeclinica, pagina);
        }
        [Authorize("Bearer")]
        [HttpGet]
        [Route("api/[controller]/listarpordatanascimento")]
        public ICollection<Obito> ListarPorDataNascimento(string datanascimento, int pagina)
        {
            return _repositoryObito.ListarPorDataNascimento(datanascimento, pagina);
        }
        [Authorize("Bearer")]
        [HttpGet]
        [Route("api/[controller]/listarpornumeroordem")]
        public ICollection<Obito> ListarPorNumeroOrdem(string numeroordem, int pagina)
        {
            return _repositoryObito.ListarPorNumeroOrdem(numeroordem, pagina);
        }
        [Authorize("Bearer")]
        [HttpGet]
        [Route("api/[controller]/listarpordatafalecimento")]
        public ICollection<Obito> ListarPelaDataDeFalecimento(string datafalecimento, int pagina)
        {
            return _repositoryObito.ListarPelaDataDeFalecimento(datafalecimento, pagina);
        }
        [Authorize("Bearer")]
        [HttpGet]
        [Route("api/[controller]/listarmedicos")]
        public ICollection<Medico> ListarMedicos(string nomemedico, int pagina)
        {
            return _repositoryObito.ListarMedicos(nomemedico, pagina);
        }

        [Authorize("Bearer")]
        [HttpGet]
        [Route("api/[controller]/listarporenomepaciente")]
        public ICollection<Obito> ListarPorNomeDoPaciente(string nomepaciente, int pagina)
        {
            return _repositoryObito.ListarPorNomePaciente(nomepaciente, pagina);
        }
        [Authorize("Bearer")]
        [HttpGet]
        [Route("api/[controller]/listarclinicas")]
        public ICollection<Clinica> ListarClinicas(string nomeclinica, int pagina)
        {
            return _repositoryObito.ListarClinicas(nomeclinica, pagina);
        }
    }
}