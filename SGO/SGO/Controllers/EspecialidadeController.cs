﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SGO.Interfaces;
using SGO.Models;

namespace SGO.Controllers
{
    [ApiController]
    public class EspecialidadeController : ControllerBase
    {
        private IRepositoryEspecialidade _repositoryEspecialidade;
        public EspecialidadeController(IRepositoryEspecialidade repositoryEspecialidade)
        {
            _repositoryEspecialidade = repositoryEspecialidade;
        }
        [HttpGet]
        [Route("api/[controller]/listarpornome")]
        [Authorize("Bearer")]
        public ICollection<Especialidade> ListarPorNome(string nome, int pagina)
        {
            return _repositoryEspecialidade.ListarPorNome(nome, pagina);
        }
        [HttpPost]
        [Route("api/[controller]/create")]
        [Authorize("Bearer")]
        public void Create(Especialidade especialidade)
        {
            _repositoryEspecialidade.Create(especialidade);
        }
        [HttpPut]
        [Route("api/[controller]/update")]
        [Authorize("Bearer")]
        public void Update(Especialidade especialidade)
        {
            _repositoryEspecialidade.Update(especialidade);
        }
    }
}