﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SGO.Interfaces;
using SGO.Models;

namespace SGO.Controllers
{
    [ApiController]
    public class CausaController : ControllerBase
    {
        private IRepositoryCausa _repositoryCausa;
        public CausaController(IRepositoryCausa repositoryCausa)
        {
            _repositoryCausa = repositoryCausa;
        }
        [HttpPost]
        [Authorize("Bearer")]
        [Route("api/[controller]/create")]
        public void Create(Causa causa)
        {
            _repositoryCausa.Create(causa);
        }
        [HttpGet]
        [Authorize("Bearer")]
        [Route("api/[controller]/listarpornome")]
        public ICollection<Causa> ListarPorNomes(string nome, int pagina)
        {
            return _repositoryCausa.ListarPorNome(nome, pagina);
        }
        [HttpGet]
        [Authorize("Bearer")]
        [Route("api/[controller]/getcausa")]
        public Causa GetCausa(int id)
        {
            return _repositoryCausa.GetCausa(id);
        }
    }
}