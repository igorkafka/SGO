﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using APISGO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using SGO.Data;
using SGO.Interfaces;
using SGO.Models;

namespace SGO.Controllers
{
 
    [ApiController]
    public class LoginController : ControllerBase
    {
        IRepositoryUser repositoryUser;
        private readonly ApplicationDbContext _context;
        public LoginController(ApplicationDbContext ctx)
        {
            _context = ctx;
            this.repositoryUser = new RepositoryUser(_context);
        }
        [AllowAnonymous]
        [HttpPost]
        [Route("api/[controller]/login")]
        public object Post(
          [FromBody]User usuario,
          [FromServices]UserManager<ApplicationUser> userManager,
          [FromServices]SignInManager<ApplicationUser> signInManager,
          [FromServices]SigningConfigurations signingConfigurations,
          [FromServices]TokenConfigurations tokenConfigurations)
        {
            bool credenciaisValidasfuncionario = false;
            bool credenciaisValidasadministrador = false;
            if (usuario != null && !String.IsNullOrWhiteSpace(usuario.UserEmail))
            {
                // Verifica a existência do usuário nas tabelas do
                // ASP.NET Core Identity 
                var userIdentity = userManager 
                    .FindByEmailAsync(usuario.UserEmail).Result;
                if (userIdentity != null)
                {
                    // Efetua o login com base no Id do usuário e sua senha
                    var resultadoLogin = signInManager
                        .CheckPasswordSignInAsync(userIdentity, usuario.Password, false)
                        .Result;
                    if (resultadoLogin.Succeeded)
                    {
                        System.Diagnostics.Debug.Write("Acesso foi encontrado");
                        // Verifica se o usuário em questão possui
                        // a role Acesso-APIAlturas
                        credenciaisValidasfuncionario = userManager.IsInRoleAsync(
                            userIdentity, Roles.ROLE_API_FUNCIONARIO).Result;
                        credenciaisValidasadministrador = userManager.IsInRoleAsync(
                          userIdentity, Roles.ROLE_API_ADMINISTRATOR).Result;
                    }
                    else
                    {
                        return new
                        {
                            authenticated = false,
                            message = " Usuário não tem a autorização"
                        };
                    }
                }
            }
            else
            {
                return new
                {
                    authenticated = false,
                    message = credenciaisValidasfuncionario
                };
            }

            if (credenciaisValidasfuncionario || credenciaisValidasadministrador)
            {
                var userresult = userManager.FindByEmailAsync(usuario.UserEmail);
                userresult.Wait();
                ClaimsIdentity identity = new ClaimsIdentity(
                    new GenericIdentity(usuario.UserEmail, "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, usuario.UserEmail)
                    }
                );
                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao.AddMonths(1);

                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = tokenConfigurations.Issuer,
                    Audience = tokenConfigurations.Audience,
                    SigningCredentials = signingConfigurations.SigningCredentials,
                    Subject = identity,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });
                var token = handler.WriteToken(securityToken);
                if(credenciaisValidasadministrador)
                return new
                {
                    id = userresult.Result.Id,
                    authenticated = true,
                    created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                    expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                    accessToken = token,
                    message = "OK",
                    permissao = Roles.ROLE_API_ADMINISTRATOR
                };
                else if (credenciaisValidasfuncionario)
                {
                    return new {
                        id = userresult.Result.Id,
                        authenticated = true,
                    created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                    expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                    accessToken = token,
                    message = "OK",
                    permissao = Roles.ROLE_API_FUNCIONARIO
                    };
                }
                else
                {
                    return new
                    {
                        authenticated = false,
                        message = "Problemas"
                    };
                }
            }
            else
            {
                return new
                {
                    authenticated = false,
                    message = credenciaisValidasadministrador
                };
            }
        }
        [Route("api/[controller]/register")]
        [Authorize("Bearer")]
        [HttpPost]
        public object Post([FromBody]RegisterUser usuario,
                [FromServices]UserManager<ApplicationUser> userManager,
                [FromServices]SignInManager<ApplicationUser> signInManager,
                [FromServices]SigningConfigurations signingConfigurations,
                [FromServices]TokenConfigurations tokenConfigurations)
        {
            ApplicationUser userIdentity = null;
            if (usuario != null && !String.IsNullOrWhiteSpace(usuario.Email))
            {
                // Verifica a existência do usuário nas tabelas do
                // ASP.NET Core Identity
                userIdentity = userManager
                   .FindByEmailAsync(usuario.Email).Result;
            }
            else
            {
                return new
                {
                    authenticated = false,
                    message = "Usuário já está cadastrado"
                };
            }
            if (userIdentity == null)
            {
                ApplicationUser applicationuser;
                string resultid = "";
                if (usuario.Perfil == Roles.ROLE_API_ADMINISTRATOR)
                {
                    var resultregisteraccount = userManager.CreateAsync(usuario, usuario.Password).Result;
                    if (resultregisteraccount.Succeeded)
                    {
                        var toatrributerole = userManager.AddToRoleAsync(usuario, Roles.ROLE_API_ADMINISTRATOR);
                        toatrributerole.Wait();
                        var user = userManager.FindByEmailAsync(usuario.Email);
                        user.Wait();
                        resultid = user.Result.Id;

                    }

                }
                else
                {
                    var resultregisteraccount = userManager.CreateAsync(usuario, usuario.Password).Result;
                    if (resultregisteraccount.Succeeded)
                    {
                        var toatrributerole = userManager.AddToRoleAsync(usuario, Roles.ROLE_API_FUNCIONARIO);
                        toatrributerole.Wait();
                        var user = userManager.FindByEmailAsync(usuario.Email);
                        user.Wait();
                        resultid = user.Result.Id;
                    }
                }
                ClaimsIdentity identity = new ClaimsIdentity(
                    new GenericIdentity(usuario.Email, "Login"),
                    new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, usuario.Email)
                    }
                );
               
                DateTime dataCriacao = DateTime.Now;
                DateTime dataExpiracao = dataCriacao.AddMonths(1);

                var handler = new JwtSecurityTokenHandler();
                var securityToken = handler.CreateToken(new SecurityTokenDescriptor
                {
                    Issuer = tokenConfigurations.Issuer,
                    Audience = tokenConfigurations.Audience,
                    SigningCredentials = signingConfigurations.SigningCredentials,
                    Subject = identity,
                    NotBefore = dataCriacao,
                    Expires = dataExpiracao
                });
                var token = handler.WriteToken(securityToken);
                if (usuario.Perfil == Roles.ROLE_API_FUNCIONARIO)
                {
                    return new
                    {
                        authenticated = true,
                        created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                        expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                        accessToken = token,
                        message = "OK",
                        permissao = Roles.ROLE_API_FUNCIONARIO,
                        id = resultid
                    };
                }
                else if (usuario.Perfil == Roles.ROLE_API_ADMINISTRATOR)
                {
                    return new
                    {
                        authenticated = true,
                        created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                        expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                        accessToken = token,
                        message = "OK",
                        permissao = Roles.ROLE_API_ADMINISTRATOR,
                        id = resultid
                    };
                }
                else
                {
                    return new
                    {
                        authenticated = false,
                        message = "Falha ao credencial"
                    };
                }
            }
            else
            {
                return new
                {
                    authenticated = false,
                    message = "Falha ao autenticar"
                };
            }
        }
        [Route("api/[controller]/listarpornome")]
        [Authorize("Bearer")]
        [HttpGet]
        public ICollection<ApplicationUser> ListarPorNome(string nome, int pagina)
        {
            return this.repositoryUser.ListarPorNome(nome, pagina);
        }
        [Route("api/[controller]/listarpordata")]
        [Authorize("Bearer")]
        [HttpGet]
        public ICollection<ApplicationUser> ListarPorData(string datanascimento, int pagina)
        {
            return this.repositoryUser.ListarPorData(datanascimento, pagina);
        }
        [Route("api/[controller]/listarporendereco")]
        [Authorize("Bearer")]
        [HttpGet]
        public ICollection<ApplicationUser> ListarPorEndereco(string endereco, int pagina)
        {
            return this.repositoryUser.ListarPorEndereco(endereco, pagina);
        }
        [Route("api/[controller]/listarporemail")]
        [Authorize("Bearer")]
        [HttpGet]
        public ICollection<ApplicationUser> ListarPorEmail(string email, int pagina)
        {
            return this.repositoryUser.ListarPorEmail(email, pagina);
        }
        [Route("api/[controller]/listarpormatricula")]
        [Authorize("Bearer")]
        [HttpGet]
        public ICollection<ApplicationUser> ListarPorMatricula(string matricula, int pagina)
        {
            return this.repositoryUser.ListarPorMatricula(matricula, pagina);
        }
        [AllowAnonymous]
        [HttpPut]
        [Authorize("Bearer")]
        [Route("api/[controller]/atualizar")]
        public bool Update(
       [FromQuery] string id,
       [FromBody]RegisterUser usuario,
       [FromServices]UserManager<ApplicationUser> userManager,
       [FromServices]TokenConfigurations tokenConfigurations)
        {
            return repositoryUser.Update(id, usuario, userManager);
        }
}
}