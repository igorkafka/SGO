using System;
using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SGO.Interfaces;
using SGO.Models;

[ApiController]
public class TaxaObitoController: ControllerBase
{
    private IRepositoryObito _repositoryobito;
    private ITaxaObitoMes _taxaObitoMes;
    private ITaxaObitoAno _taxaObitoAno;
    private IRepositoryCausa _repositoryCausa;
    private ITaxaObitoCausa _taxaObitoCausa;
    private IRepositoryClinica _repositoryClinica;
    private ITaxaObitoClinica _taxaObitoClinica;
    private ITaxaObitoSexo _taxaObitoSexo;
    public TaxaObitoController(IRepositoryCausa repositoryCausa, IRepositoryClinica repositoryClinica, ITaxaObitoCausa taxaObitoCausa, IRepositoryObito repositoryobito,
        ITaxaObitoAno taxaObitoAno  ,ITaxaObitoMes taxaObitoMes, ITaxaObitoClinica taxaObitoClinica, ITaxaObitoSexo taxaObitoSexo)
    {
        _repositoryobito = repositoryobito;
        _taxaObitoMes = taxaObitoMes;
        _taxaObitoAno = taxaObitoAno;
        _repositoryCausa = repositoryCausa;
        _taxaObitoCausa = taxaObitoCausa;
        _taxaObitoClinica = taxaObitoClinica;
        _repositoryClinica = repositoryClinica;
        _taxaObitoSexo = taxaObitoSexo;
    }
    [HttpGet]
    [Route("api/[controller]/listartaxaporanos")]
    [Authorize("Bearer")]
    public IList<TaxaObitoAno> ListarTaxaPorAnos(int anoinicial, int anofinal)
    {
      
      return _taxaObitoAno.GetDadosObitosAno(_repositoryobito.ListarPorFaixaDeAnos(anoinicial, anofinal),anoinicial, anofinal);
    }
    [HttpGet]
    [Route("api/[controller]/listartaxapormeses")]
    [Authorize("Bearer")]
    public ICollection<TaxaObitoMes> ListarTaxaPorMeses(int ano)
    {
        return _taxaObitoMes.GetDadosObitosMes(_repositoryobito.ListarPorFaixaDeMeses(ano), ano);
    }
    [HttpGet]
    [Route("api/[controller]/taxacausa")]
    [Authorize("Bearer")]
    public TaxaObitoCausa TaxaCausa(int idcausa)
    {
       _taxaObitoCausa.Causa = _repositoryCausa.GetCausa(idcausa);
      return  _taxaObitoCausa.GetDadosObitosCausa(this._repositoryobito.TodosObitosPorCausa(idcausa));
    }
    [HttpGet]
    [Route("api/[controller]/taxaclinica")]
    [Authorize("Bearer")]
    public ICollection<TaxaObitoClinica> TaxaClinica(int ano)
    {
        return _taxaObitoClinica.GetDadosObitosClinica(_repositoryClinica.Top10ClinicaComMaisObito(ano));
    }
    [HttpGet]
    [Route("api/[controller]/taxacausafrequente")]
    [Authorize("Bearer")]
    public ICollection<TaxaObitoCausa> TaxaCausaFrequente(int ano)
    {
        return _taxaObitoCausa.GetDadosObitosCausaFrequente(_repositoryCausa.GetCausasFrequentes(ano), ano);
    }
    [HttpGet]
    [Route("api/[controller]/taxagenero")]
    [Authorize("Bearer")]
    public ICollection<TaxaObitoCausa> TaxaCausaGenero(bool genero)
    {
        return _taxaObitoCausa.GetDadosObitosCausaFrequente(_repositoryCausa.GetCausaSexo(genero),20);
    }
}