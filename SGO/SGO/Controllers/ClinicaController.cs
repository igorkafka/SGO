﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SGO.Data;
using SGO.Interfaces;
using SGO.Models;

namespace SGO.Controllers
{
    [ApiController]
    public class ClinicaController : ControllerBase
    {
        IRepositoryClinica _repositoryClinica;
        public ClinicaController(IRepositoryClinica repositoryClinica)
        {
            _repositoryClinica = repositoryClinica;
        }
        [HttpPost]
        [Authorize("Bearer")]
        [Route("api/[controller]/create")]
        public void Create(Clinica clinica)
        {
            _repositoryClinica.Create(clinica);
        }
        [HttpPut]
        [Authorize("Bearer")]
        [Route("api/[controller]/update")]
        public void Update(int id ,Clinica clinica)
        {
            _repositoryClinica.Update(id, clinica);
        }
        [HttpGet]
        [Authorize("Bearer")]
        [Route("api/[controller]/listarpornome")]
        public ICollection<Clinica> ListarPorNomes(string nome, int pagina)
        {
           return _repositoryClinica.ListarPorNome(nome, pagina);
        }
        [HttpGet]
        [Authorize("Bearer")]
        [Route("api/[controller]/listarpordescricao")]
        public ICollection<Clinica> ListarPorDescricao(string descricao, int pagina)
        {
           return _repositoryClinica.ListarPorDescricao(descricao, pagina);
        }
    }
}