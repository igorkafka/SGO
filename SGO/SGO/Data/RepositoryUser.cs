﻿using APISGO;
using Microsoft.AspNetCore.Identity;
using SGO.Interfaces;
using SGO.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Data
{
    public class RepositoryUser : IMultiplicadorPagina, IRepositoryUser
    {
        private RepositoryRole repositoryRole;
        private RepositoryUserRole RepositoryUserRole;
        private ApplicationDbContext db { get; set; }
        public RepositoryUser(ApplicationDbContext applicationDbContext)
        {
            db = applicationDbContext;
            RepositoryUserRole = new RepositoryUserRole(db);
            repositoryRole = new RepositoryRole(db);
        }
        public void Create(ApplicationUser usuario)
        {
            db.Users.Add(usuario);
            db.SaveChanges();
        }
        public ICollection<ApplicationUser> ListarPorNome(string nome, int pagina)
        {
            return this.db.Users.Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.UserName.Contains(nome)).ToList();
        }
        public ICollection<ApplicationUser> ListarPorData(string datanascimento, int pagina)
        {
            return this.db.Users.Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.DataNascimento.Date == Convert.ToDateTime(datanascimento).Date).ToList();
        }
        public ICollection<ApplicationUser> ListarPorEndereco(string endereco, int pagina)
        {
            return this.db.Users.Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.Endereco.Contains(endereco)).ToList();
        }
        public ICollection<ApplicationUser> ListarPorMatricula(string matricula, int pagina)
        {
            return this.db.Users.Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.Matricula == matricula).ToList();
        }
        public ICollection<ApplicationUser> ListarPorEmail(string email, int pagina)
        {
            return this.db.Users.Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.Email.Contains(email)).ToList();
        }

        public int MultiplicaPagina(int pagina)
        {
            return 14 * Math.Abs((pagina - 1));
        }
        public bool Update(string id ,RegisterUser applicationUser, UserManager<ApplicationUser> userManager)
        {
            try
            {
                ApplicationUser userupdated = db.Users.Where(x => x.Id == id).FirstOrDefault();
 
                    userupdated.Matricula = applicationUser.Matricula;
                    userupdated.UserName = applicationUser.UserName;
                    userupdated.Email = applicationUser.Email;
                    userupdated.DataNascimento = applicationUser.DataNascimento;
                    userupdated.Endereco = applicationUser.Endereco;
                    db.Users.Update(userupdated);
                    db.SaveChanges();
                   var removepassword = userManager.RemovePasswordAsync(userupdated);
                    removepassword.Wait();
                    var updatedpassword = userManager.AddPasswordAsync(userupdated, applicationUser.Password); 
                    updatedpassword.Wait(); 
                return true;
            }
            catch(Exception e)
            {
                return false; 
            }

        }
    }
}
