﻿using Microsoft.AspNetCore.Identity;
using SGO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Data
{
    public class RepositoryRole
    {
        private ApplicationDbContext db { get; set; }
        public RepositoryRole(ApplicationDbContext applicationDbContext)
        {
            db = applicationDbContext;
        }
        public IdentityRole GetRoleById(string id)
        {
          return db.Roles.Where(x => x.Id == id).FirstOrDefault();
        }
        public IdentityRole GetRoleByName(string nome)
        {
            return db.Roles.Where(x => x.Name == nome).FirstOrDefault();
        }
    }
}
