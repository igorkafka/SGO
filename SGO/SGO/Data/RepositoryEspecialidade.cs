﻿using SGO.Interfaces;
using SGO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Data
{
    public class RepositoryEspecialidade : IRepositoryEspecialidade, MultiplicadorPaginas
    {
        private ApplicationDbContext db;
        public RepositoryEspecialidade(ApplicationDbContext applicationDbContext)
        {
            db = applicationDbContext;
        }
        public void Create(Especialidade especialidade)
        {
            db.Add(especialidade);
            db.SaveChanges();
        }

        public ICollection<Especialidade> ListarPorNome(string nome, int pagina)
        {
            return db.Especialidades.Where(x => x.Nome.Contains(nome)).Take(14).Skip(MultiplicaPagina(pagina)).ToList();
        }

        public int MultiplicaPagina(int pagina)
        {
            return 14 * Math.Abs((pagina - 1));
        }

        public void Update(Especialidade especialidade)
        {
            Especialidade especialidadeatualizar = db.Especialidades.Where(x => x.Id == especialidade.Id).FirstOrDefault();
            especialidadeatualizar.Nome = especialidade.Nome;
            db.Update(especialidadeatualizar);
            db.SaveChanges();
        }
    }
}
