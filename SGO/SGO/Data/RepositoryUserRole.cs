﻿using Microsoft.AspNetCore.Identity;
using SGO.Interfaces;
using SGO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Data
{
    public class RepositoryUserRole : IRepositoryUserRole
    {
        private ApplicationDbContext db { get; set; }
        public RepositoryUserRole(ApplicationDbContext applicationDbContext)
        {
            this.db = applicationDbContext;
        }
        public IdentityUserRole<string> GetUserRoleById(string id)
        {
           return db.UserRoles.Where(x => x.UserId == id).FirstOrDefault();
        }
        public void Delete(IdentityUserRole<string> identityUserRole)
        {
            db.Set<IdentityUserRole<string>>().Remove(identityUserRole);
            db.SaveChanges();
        }
    }
}
