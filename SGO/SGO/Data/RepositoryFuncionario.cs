﻿using SGO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Data
{
    public class RepositoryFuncionario: MultiplicadorPaginas
    {
        private ApplicationDbContext db { get; set; }
        public RepositoryFuncionario(ApplicationDbContext applicationDbContext)
        {
            db = applicationDbContext;
        }
        public void Create(ApplicationUser user)
        {
            this.db.Users.Add(user);
            this.db.SaveChanges();
        }
        public ICollection<ApplicationUser> ListarPorNome(string nome, int pagina)
        {
            return db.Users.Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.UserName.Contains(nome)).ToList();
        }
        public ICollection<ApplicationUser> ListarPorData(DateTime datanascimento, int pagina)
        {
            return db.Users.Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.DataNascimento == datanascimento).ToList();
        }
        public int MultiplicaPagina(int pagina)
        {
            return 14 *  Math.Abs((pagina -1));
        }
    }
}
