using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SGO.Interfaces;
using SGO.Models;

namespace SGO.Data
{
    public class RepositoryObito : MultiplicadorPaginas, IRepositoryObito
    {
        private ApplicationDbContext db { get; set; }
        public RepositoryObito(ApplicationDbContext applicationDbContext)
        {
            db = applicationDbContext;
        }

        public void Create(Obito obito)
        {
            obito.Medico = db.Medicos.Where(x => x.Id == obito.Medico.Id).FirstOrDefault();
            obito.Funcionario = db.Users.Where(x => x.Id == obito.Funcionario.Id).FirstOrDefault();
            obito.Clinica = db.Clinicas.Where(x => x.Id == obito.Clinica.Id).FirstOrDefault();
            obito.Causa = db.Causas.Where(x => x.Id == obito.Causa.Id).FirstOrDefault();
            db.Obitos.Add(obito);
            db.SaveChanges();
        }
        public void Update(int id, Obito obito)
        {
             Paciente pacienteatualizar = db.Pacientes.Where(x => x.Id == obito.Paciente.Id).FirstOrDefault();
            pacienteatualizar.Nome = obito.Paciente.Nome;
            pacienteatualizar.NumeroRegistro = obito.Paciente.NumeroRegistro;
            pacienteatualizar.Sexo = obito.Paciente.Sexo;
            pacienteatualizar.Endereco = obito.Paciente.Endereco;
            Obito obitoatualizar = db.Obitos.Where(x => x.NumeroOrdem == obito.NumeroOrdem).FirstOrDefault();
            obitoatualizar.Paciente = pacienteatualizar;
            obitoatualizar.Clinica = db.Clinicas.Where(x => x.Id == obito.Clinica.Id).FirstOrDefault();
            obitoatualizar.DataFalecimento = obito.DataFalecimento;
            obitoatualizar.Medico = db.Medicos.Where(x => x.Id == obito.Medico.Id).FirstOrDefault();
            obitoatualizar.Numeracao = obito.Numeracao;
            obitoatualizar.NumeroCaixa = obito.NumeroCaixa;
            obitoatualizar.NumeroOrdem = obito.NumeroOrdem;
            Causa causa = db.Causas.Where(x => x.Id == obito.Causa.Id).FirstOrDefault();
            obitoatualizar.Causa = causa;
            db.Update(pacienteatualizar);
            db.Update(obitoatualizar);
            db.SaveChanges();
        }
        public Obito GetObito(int numeroordem)
        {
            return db.Obitos.Where(x => x.NumeroOrdem == numeroordem).Include(x => x.Medico).
                Include(x => x.Paciente)
                .FirstOrDefault();
        }
        public int MultiplicaPagina(int pagina)
        {
            return 10 * Math.Abs((pagina - 1));
        }

        public ICollection<Obito> ListarPelaDataDeFalecimento(string datafalecimento, int pagina)
        {
            return db.Obitos.Where(x => x.DataFalecimento.Date == Convert.ToDateTime(datafalecimento).Date)
                .Include(x => x.Paciente).Include(x => x.Medico)
                          .Include(x => x.Clinica).Include(x => x.Funcionario).
                          Include(x => x.Causa).Take(10).Skip(MultiplicaPagina(pagina)).ToList();
        }

        public ICollection<Obito> ListarPelaNumeracao(string numeracao, int pagina)
        {
            return db.Obitos.Where(x => x.Numeracao == numeracao).Include(x => x.Paciente).Include(x => x.Medico)
                  .Include(x => x.Clinica).Include(x => x.Funcionario).Include(x => x.Causa).Take(10).
                  Skip(MultiplicaPagina(pagina)).ToList();
        }

        public ICollection<Obito> ListaPorCaixa(string caixa, int pagina)
        {
            return db.Obitos.Where(x => x.NumeroCaixa == caixa).Include(x => x.Paciente).Include(x => x.Medico)
                   .Include(x => x.Clinica).Include(x => x.Funcionario).Include(x => x.Causa).Take(10)
                   .Skip(MultiplicaPagina(pagina)).ToList();
        }

        public ICollection<Obito> ListarPorNumeroRegistro(string numeroregistro, int pagina)
        {
            return db.Obitos.Where(x => x.Paciente.NumeroRegistro == numeroregistro).Include(x => x.Paciente).
                Include(x => x.Medico).Include(x => x.Clinica).Include(x => x.Funcionario).
                Include(x => x.Causa).Take(10).Skip(MultiplicaPagina(pagina)).ToList();
        }

        public ICollection<Obito> ListarPorDataNascimento(string datanascimento, int pagina)
        {
            return db.Obitos.Where(x => x.Paciente.DataNascimento.Date ==  Convert.ToDateTime(datanascimento).Date).
                Include(x => x.Paciente).Include(x => x.Medico).Include(x => x.Clinica)
                .Include(x => x.Funcionario).Include(x => x.Causa).Take(10).Skip(MultiplicaPagina(pagina)).ToList();
        }

        public ICollection<Obito> ListarPorNomeDaClinica(string nomeclinca, int pagina)
        {
            return db.Obitos.Where(x => x.Clinica.Nome.Contains(nomeclinca)).Include(x => x.Paciente).Include(x => x.Medico)
                   .Include(x => x.Clinica).Include(x => x.Funcionario).Include(x => x.Causa).
                   Take(10).Skip(MultiplicaPagina(pagina)).ToList();
        }

        public ICollection<Obito> ListarPorNomeDoMedico(string nomemedico, int pagina)
        {
            return db.Obitos.Where(x => x.Medico.Nome.Contains(nomemedico)).Include(x => x.Paciente)
                .Include(x => x.Medico).Include(x => x.Clinica).Include(x => x.Causa).
                Take(10).Skip(MultiplicaPagina(pagina)).ToList();
        }
        public ICollection<Medico> ListarMedicos(string nomemedico, int pagina)
        {
            return db.Medicos.Where(x => x.Nome.Contains(nomemedico)).Include(x => x.Especialidade).
                Skip(5 * Math.Abs((pagina - 1))).Take(5).ToList();
        }
        public ICollection<Clinica> ListarClinicas(string nomeclinica, int pagina)
        {
            return db.Clinicas.Where(x => x.Nome.Contains(nomeclinica)).Skip(5 * Math.Abs((pagina - 1))).
                Take(5).ToList();
        }
        public ICollection<Obito> ListarPorNomePaciente(string nomepaciente, int pagina)
        {
            return db.Obitos.Where(x => x.Paciente.Nome.Contains(nomepaciente))
                .Include(x => x.Paciente).Include(x => x.Medico).
                Include(x => x.Clinica).Include(x => x.Causa).Skip(MultiplicaPagina(pagina)).Take(10).ToList();
        }

        public ICollection<Obito> ListarPorNumeroOrdem(string numeroordem, int pagina)
        {
            return db.Obitos.Where(x => x.NumeroOrdem == int.Parse(numeroordem)).Include(x => x.Paciente).
                Include(x => x.Medico). Include(x => x.Clinica).Include(x => x.Causa).Skip(MultiplicaPagina(pagina)).
                           Take(10).ToList();
        }
        public IList<Obito> ListarPorFaixaDeAnos(int anoinicial, int anofinal)
        {
            return db.Obitos.Where(x => x.DataFalecimento.Year >= anoinicial && x.DataFalecimento.Year <= anofinal).
                ToList();
        }
        public IList<Obito> ListarPorFaixaDeMeses(int ano)
        {
         return db.Obitos.Where(x => x.DataFalecimento.Year == ano).ToList();
        }
        public IList<Obito> ListarPorCausa(string nomecausa, int pagina)
        {
            return db.Obitos.Where(x => x.Causa.Nome.Contains(nomecausa)).Take(10)
                .Skip(MultiplicaPagina(pagina)).Include(x => x.Causa).ToList();
        }
        public IList<Obito> TodosObitosPorCausa(int idcausa)
        {
            return db.Obitos.Where(x => x.Causa.Id == idcausa).Include(x => x.Causa).ToList();
         }
    }
}