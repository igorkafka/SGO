﻿using Microsoft.EntityFrameworkCore;
using SGO.Interfaces;
using SGO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Data
{
    public class RepositoryMedico : MultiplicadorPaginas, IRepositoryMedico
    {
        public int MultiplicaPagina(int pagina)
        {
            return 14 * Math.Abs((pagina - 1));
        }
        private ApplicationDbContext db { get; set; }
        public RepositoryMedico(ApplicationDbContext applicationDbContext)
        {
            db = applicationDbContext;
        }
        public void Create(Medico medico)
        {
            medico.Especialidade = db.Especialidades.Where(x => x.Id == medico.Especialidade.Id).FirstOrDefault();
            this.db.Medicos.Add(medico);
            this.db.SaveChanges();
        }
        public void Update(int id, Medico medico)
        {
            Medico medicoatualizar = db.Medicos.Where(x => x.Id == medico.Id).FirstOrDefault();
            medicoatualizar.Nome = medico.Nome;
            medicoatualizar.Crm = medico.Crm;
            medicoatualizar.DataNascimento = medico.DataNascimento;
            medicoatualizar.Especialidade = db.Especialidades.Where(x => x.Id == medico.Especialidade.Id).FirstOrDefault();
            db.Update(medicoatualizar);
            db.SaveChanges();
        }
        public ICollection<Medico> ListarPorNome(string nome, int pagina)
        {
            return db.Medicos.Include(x => x.Especialidade).Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.Nome.Contains(nome)).ToList();
        }
        public ICollection<Medico> ListarPorData(string datanascimento, int pagina)
        {
            return db.Medicos.Include(x => x.Especialidade).Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.DataNascimento.Date == Convert.ToDateTime(datanascimento).Date).ToList();
        }
        public ICollection<Medico> ListarPorEspecialidade(string especialidade, int pagina)
        {
            return db.Medicos.Include(x => x.Especialidade).Skip(MultiplicaPagina(pagina)).Take(14).Include(m => m.Especialidade).Where(x => x.Especialidade.Nome.Contains(especialidade)).ToList();
        }
        public ICollection<Medico> ListarPorCrm(string crm, int pagina)
        {
            return db.Medicos.Include(x => x.Especialidade).Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.Crm.Contains(crm)).ToList();
        }
    }
}
