﻿using Microsoft.EntityFrameworkCore;
using SGO.Interfaces;
using SGO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Data
{
    public class RepositoryClinica : MultiplicadorPaginas, IRepositoryClinica
    {
        private ApplicationDbContext db { get; set; }
        public RepositoryClinica(ApplicationDbContext applicationDbContext)
        {
            db = applicationDbContext;
        }
        public void Create(Clinica clinica)
        {
            db.Clinicas.Add(clinica);
            db.SaveChanges();
        }
        public void Update(int id, Clinica clinica)
        {
            Clinica clinicaatualizar = db.Clinicas.Where(x => x.Id == id).FirstOrDefault();
            clinicaatualizar.Nome = clinica.Nome;
            clinicaatualizar.Descricao = clinica.Descricao;
            db.Update(clinicaatualizar);
            db.SaveChanges();
        }
        public ICollection<Clinica> ListarPorNome(string nome, int pagina)
        {
            return db.Clinicas.Skip(MultiplicaPagina(pagina)).Take(14).Where(x => x.Nome.Contains(nome)).ToList();
        }
        public ICollection<Clinica> ListarPorDescricao(string descricao, int pagina)
        {
            return db.Clinicas.Take(14).Skip(MultiplicaPagina(pagina)).
                Where(x => x.Descricao.Contains(descricao)).ToList();
        }
        public int MultiplicaPagina(int pagina)
        {
            return 14 * Math.Abs((pagina - 1));
        }
        public IList<Clinica> Top10ClinicaComMaisObito(int ano)
        {
            return db.Clinicas.OrderByDescending(c => c.Obitos.Count).
                Take(10).Where(x => x.Obitos.Any(o => o.DataFalecimento.Year == ano)).
                Include(x => x.Obitos).ToList();
        } 
    }
}
