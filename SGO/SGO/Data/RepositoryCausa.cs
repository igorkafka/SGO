﻿using Microsoft.EntityFrameworkCore;
using SGO.Interfaces;
using SGO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Data
{
    public class RepositoryCausa : MultiplicadorPaginas, IRepositoryCausa
    {
        private ApplicationDbContext db { get; set; }
        public RepositoryCausa(ApplicationDbContext applicationDbContext)
        {
            db = applicationDbContext;
        }
        public void Create(Causa causa)
        {
            db.Causas.Add(causa);
            db.SaveChanges();
        }
        public Causa GetCausa(int id)
        {
            return this.db.Causas.Where(x => x.Id == id).Include(x => x.Obitos).FirstOrDefault();
        }
        public void Update(Causa causa)
        {
            Causa causaatualizar = db.Causas.Where(x => x.Id == causa.Id).FirstOrDefault();
            causaatualizar.Nome = causa.Nome;
            db.SaveChanges();
        }
        public ICollection<Causa> ListarPorNome(string nome, int pagina)
        {
           return db.Causas.Where(x => x.Nome.Contains(nome)).Take(14).Skip(MultiplicaPagina(pagina)).ToList();
        }
        public IList<Causa> GetCausasFrequentes(int ano)
        {
            return db.Causas.OrderByDescending(c => c.Obitos.Count).Take(10).
                Where(x => x.Obitos.Any(o => o.DataFalecimento.Year == ano)).Include(x => x.Obitos).ToList();
        }
        public IList<Causa> GetCausaSexo(bool sexo)
        {
            return db.Causas.OrderByDescending(c => c.Obitos.Count).Take(10).
                Where(x => x.Obitos.Any(o => o.Paciente.Sexo == sexo)).Include(x => x.Obitos).ToList();
        }
        public IList<Causa> GetCausaIdade(int idcausa)
        {
            return db.Causas.OrderByDescending(c => c.Obitos.Count).Take(10).
                Where(x => x.Id == idcausa).Include(x => x.Obitos).ToList();
        }
        public int MultiplicaPagina(int pagina)
        {
            return 14 * Math.Abs((pagina - 1));
        }
    }
}
