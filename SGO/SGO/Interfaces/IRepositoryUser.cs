﻿using System.Collections.Generic;
using APISGO;
using Microsoft.AspNetCore.Identity;
using SGO.Models;

namespace SGO.Interfaces
{
    public interface IRepositoryUser
    {
        void Create(ApplicationUser usuario);
        ICollection<ApplicationUser> ListarPorData(string datanascimento, int pagina);
        ICollection<ApplicationUser> ListarPorEmail(string email, int pagina);
        ICollection<ApplicationUser> ListarPorEndereco(string endereco, int pagina);
        ICollection<ApplicationUser> ListarPorMatricula(string matricula, int pagina);
        ICollection<ApplicationUser> ListarPorNome(string nome, int pagina);
        bool Update(string id,RegisterUser applicationUser, UserManager<ApplicationUser> userManager);
    }
}