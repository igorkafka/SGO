﻿using SGO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Interfaces
{
    public interface IRepositoryEspecialidade
    {
        void Create(Especialidade especialidade);
        ICollection<Especialidade> ListarPorNome(string nome, int pagina);
        void Update(Especialidade especialidade);
    }
}
