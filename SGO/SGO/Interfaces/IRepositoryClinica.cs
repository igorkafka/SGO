﻿using System.Collections.Generic;
using SGO.Models;

namespace SGO.Interfaces
{
    public interface IRepositoryClinica
    {
        void Create(Clinica clinica);
        ICollection<Clinica> ListarPorDescricao(string nome, int pagina);
        ICollection<Clinica> ListarPorNome(string nome, int pagina);
        void Update(int id, Clinica clinica);
        IList<Clinica> Top10ClinicaComMaisObito(int id);
    }
}