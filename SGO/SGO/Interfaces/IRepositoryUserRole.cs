﻿using Microsoft.AspNetCore.Identity;

namespace SGO.Interfaces
{
    public interface IRepositoryUserRole
    {
        IdentityUserRole<string> GetUserRoleById(string id);
    }
}