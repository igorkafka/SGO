﻿using SGO.Models;
using System.Collections.Generic;

namespace SGO.Interfaces
{
    public interface ITaxaObitoSexo
    {
        ICollection<TaxaObitoSexo> GetDadosObitosSexo(Causa causa);
    }
}