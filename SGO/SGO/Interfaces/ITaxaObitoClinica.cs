﻿using SGO.Models;
using System.Collections.Generic;

namespace SGO.Interfaces
{
    public interface ITaxaObitoClinica
    {
        IList<TaxaObitoClinica> GetDadosObitosClinica(IList<Clinica> clinicas);
    }
}