﻿using SGO.Models;
using System.Collections.Generic;

namespace SGO.Interfaces
{
    public interface ITaxaObitoMes
    {
        int Ano { get; set; }
        int Mes { get; set; }

        ICollection<TaxaObitoMes> GetDadosObitosMes(IList<Obito> obitos, int ano);
    }
}