﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    interface IPessoa
    {
         bool Sexo { get; set; }
         DateTime DataNascimento { get; set; }
    }
}
