﻿using System;
using System.Collections.Generic;
using SGO.Models;

namespace SGO.Interfaces
{
    public interface IRepositoryObito
    {
        void Create(Obito obito);
        Obito GetObito(int id);
        ICollection<Obito> ListarPelaDataDeFalecimento(string datafalecimento, int pagina);
        ICollection<Obito> ListarPelaNumeracao(string numeracao, int pagina);
        ICollection<Obito> ListaPorCaixa(string caixa, int pagina);
        ICollection<Obito> ListarPorNumeroOrdem(string numeroordem, int pagina);
        ICollection<Obito> ListarPorNumeroRegistro(string numeroregistro, int pagina);
        ICollection<Obito> ListarPorDataNascimento(string datanascimento, int pagina);
        ICollection<Obito> ListarPorNomeDaClinica(string nomeclinca, int pagina);
        ICollection<Obito> ListarPorNomeDoMedico(string nomemedico, int pagina);
        ICollection<Obito> ListarPorNomePaciente(string nomepaciente, int pagina);
        ICollection<Medico> ListarMedicos(string nomemedico, int pagina);
        ICollection<Clinica> ListarClinicas(string nomeclinica, int pagina);
        IList<Obito> ListarPorFaixaDeAnos(int anoinicial, int anofinal);
        IList<Obito> ListarPorFaixaDeMeses(int ano);
        void Update(int id, Obito obito);
        IList<Obito> TodosObitosPorCausa(int idcausa);
        IList<Obito> ListarPorCausa(string nomecausa, int pagina);
    }
}