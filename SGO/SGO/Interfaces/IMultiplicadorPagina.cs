﻿using SGO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Interfaces
{
    interface IMultiplicadorPagina
    {
        int MultiplicaPagina(int pagina);
    }
}
