﻿using SGO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Interfaces
{
   public interface ITaxaObitoCausa
    {
         Causa Causa { get; set; }
         TaxaObitoCausa GetDadosObitosCausa(IList<Obito> obitos);
        IList<TaxaObitoCausa> GetDadosObitosCausaFrequente(IList<Causa> causas, int ano);
    }
}
