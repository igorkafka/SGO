﻿using System.Collections.Generic;
using SGO.Models;

namespace SGO.Interfaces
{
    public interface IRepositoryCausa
    {
        void Create(Causa causa);
        ICollection<Causa> ListarPorNome(string nome, int pagina);
        int MultiplicaPagina(int pagina);
        void Update(Causa causa);
        Causa GetCausa(int id);
        IList<Causa> GetCausasFrequentes(int ano);
        IList<Causa> GetCausaSexo(bool sexo);
    }
}