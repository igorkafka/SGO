﻿using System;
using System.Collections.Generic;
using SGO.Models;

namespace SGO.Interfaces
{
    public interface IRepositoryMedico
    {
        void Create(Medico medico);
        ICollection<Medico> ListarPorCrm(string crm, int pagina);
        ICollection<Medico> ListarPorData(string datanascimento, int pagina);
        ICollection<Medico> ListarPorNome(string nome, int pagina);
        ICollection<Medico> ListarPorEspecialidade(string especialidade, int pagina);
        void Update(int id, Medico medico);
    }
}