﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Clinica> Clinicas { get; set; }
        public DbSet<Medico> Medicos { get; set; }
        public DbSet<Causa> Causas { get; set; }
        public DbSet<Especialidade> Especialidades { get; set; }
        public DbSet<Obito> Obitos { get; set; }
        public DbSet<Paciente> Pacientes { get; set; }
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        public ApplicationDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Obito>()
      .HasOne(o => o.Paciente)
      .WithOne(p => p.Obito)
      .HasForeignKey<Paciente>(p => p.ObitoId);
            base.OnModelCreating(builder);
        }
    }
}
