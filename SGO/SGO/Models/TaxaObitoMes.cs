﻿using SGO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class TaxaObitoMes : TaxaObito, ITaxaObitoMes
    {
        public int Mes { get; set; }
        public int Ano { get; set; }
        public TaxaObitoMes()
        {
                
        }
        public ICollection<TaxaObitoMes> GetDadosObitosMes(IList<Obito> obitos, int ano)
        {
            IList<TaxaObitoMes> taxaObitosMeses = new List<TaxaObitoMes>();
            TaxaObitoMes taxaObitoMesJaneiro = new TaxaObitoMes();
            taxaObitoMesJaneiro.Ano = ano;
            taxaObitoMesJaneiro.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 1).Count();
            taxaObitosMeses.Add(taxaObitoMesJaneiro);
            TaxaObitoMes taxaObitoMesFevereiro = new TaxaObitoMes();
            taxaObitoMesFevereiro.Ano = ano;
            taxaObitoMesFevereiro.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 2).Count();
            taxaObitosMeses.Add(taxaObitoMesFevereiro);
            TaxaObitoMes taxaObitoMesMarco = new TaxaObitoMes();
            taxaObitoMesMarco.Ano = ano;
            taxaObitoMesMarco.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 3).Count();
            taxaObitosMeses.Add(taxaObitoMesMarco);
            TaxaObitoMes taxaObitoMesAbril = new TaxaObitoMes();
            taxaObitoMesAbril.Ano = ano;
            taxaObitoMesAbril.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 4).Count();
            taxaObitosMeses.Add(taxaObitoMesAbril);
            TaxaObitoMes taxaObitoMesMaio = new TaxaObitoMes();
            taxaObitoMesMaio.Ano = ano;
            taxaObitoMesMaio.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 5).Count();
            taxaObitosMeses.Add(taxaObitoMesMaio);
            TaxaObitoMes taxaObitoMesJunho = new TaxaObitoMes();
            taxaObitoMesJunho.Ano = ano;
            taxaObitoMesJunho.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 6).Count();
            taxaObitosMeses.Add(taxaObitoMesJunho);
            TaxaObitoMes taxaObitoMesJulho = new TaxaObitoMes();
            taxaObitoMesJulho.Ano = ano;
            taxaObitoMesJulho.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 7).Count();
            taxaObitosMeses.Add(taxaObitoMesJulho);
            TaxaObitoMes taxaObitoMesAgosto = new TaxaObitoMes();
            taxaObitoMesAgosto.Ano = ano;
            taxaObitoMesAgosto.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 8).Count();
            taxaObitosMeses.Add(taxaObitoMesAgosto);
            TaxaObitoMes taxaObitoMesSetembro = new TaxaObitoMes();
            taxaObitoMesSetembro.Ano = ano;
            taxaObitoMesSetembro.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 9).Count();
            taxaObitosMeses.Add(taxaObitoMesSetembro);
            TaxaObitoMes taxaObitoMesOutubro = new TaxaObitoMes();
            taxaObitoMesOutubro.Ano = ano;
            taxaObitoMesOutubro.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 10).Count();
            taxaObitosMeses.Add(taxaObitoMesOutubro);
            TaxaObitoMes taxaObitoMesNovembro = new TaxaObitoMes();
            taxaObitoMesNovembro.Ano = ano;
            taxaObitoMesNovembro.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 11).Count();
            taxaObitosMeses.Add(taxaObitoMesNovembro);
            TaxaObitoMes taxaObitoMesDezembro = new TaxaObitoMes();
            taxaObitoMesDezembro.Ano = ano;
            taxaObitoMesDezembro.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Month == 12).Count();
            taxaObitosMeses.Add(taxaObitoMesDezembro);
            return taxaObitosMeses;
        }
    }
}
