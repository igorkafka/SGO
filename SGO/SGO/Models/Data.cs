﻿using SGO.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace APISGO
{
    public class User
    {
        public string UserEmail { get; set; }
        public string Password { get; set; }
    }

    public static class Roles
    {
        public const string ROLE_API_FUNCIONARIO = "funcionario";
        public const string ROLE_API_ADMINISTRATOR = "administrator";
    }

    public class TokenConfigurations
    {
        public string Audience { get; set; }
        public string Issuer { get; set; }
        public int Seconds { get; set; }
    }
    public class RegisterUser:ApplicationUser
    {
        public string Perfil { get; set; }
        public string Password { get; set; }
    }

}