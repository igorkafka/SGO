﻿using SGO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class TaxaObitoAno: TaxaObito, ITaxaObitoAno
    {
        public int ano { get; set; }
        public IList<TaxaObitoAno> GetDadosObitosAno(IList<Obito> obitos, int anoinicial, int anofinal)
        {
            IList<TaxaObitoAno> listataxaObitoAnos = new List<TaxaObitoAno>();
            while (anofinal >= anoinicial)
            {
                TaxaObitoAno taxaObitoAno = new TaxaObitoAno();
                taxaObitoAno.ano = anoinicial;
                taxaObitoAno.QuantidadeDeObitos = obitos.Where(x => x.DataFalecimento.Year == anoinicial).Count();
                listataxaObitoAnos.Add(taxaObitoAno);
                anoinicial++;
            }
            return listataxaObitoAnos;
        }
    }
}
