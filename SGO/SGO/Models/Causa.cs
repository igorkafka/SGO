﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class Causa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<Obito> Obitos { get; set; }
    }
}
