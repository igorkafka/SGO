﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    
    public class Medico:Pessoa
    {
        public string Crm { get; set; }
        public Especialidade Especialidade { get; set; }
        public virtual ICollection<Obito> Obitos { get; set; }
    }
}
