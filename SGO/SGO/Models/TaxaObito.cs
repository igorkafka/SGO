﻿using SGO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public abstract class TaxaObito : ITaxaObito
    {
        public int QuantidadeDeObitos { get; set; }

    }
}
