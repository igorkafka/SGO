﻿using SGO.Models;
using System.Collections.Generic;

namespace SGO.Interfaces
{
    public interface ITaxaObitoAno
    {
        IList<TaxaObitoAno> GetDadosObitosAno(IList<Obito> obitos, int anoinicial, int anofinal);
    }
}