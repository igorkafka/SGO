﻿using SGO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class TaxaObitoClinica : TaxaObito, ITaxaObitoClinica
    {
        public Clinica Clinica { get; set; }
        public IList<TaxaObitoClinica> GetDadosObitosClinica(IList<Clinica> clinicas)
        {
            IList<TaxaObitoClinica> listataxaObitoClinicas = new List<TaxaObitoClinica>();
            foreach (Clinica clinica in clinicas)
            {
                TaxaObitoClinica taxaObitoClinica = new TaxaObitoClinica();
                taxaObitoClinica.QuantidadeDeObitos = clinica.Obitos.Count;
                taxaObitoClinica.Clinica = clinica;
                listataxaObitoClinicas.Add(taxaObitoClinica);
            }
            return listataxaObitoClinicas;
        }
    }
}
