﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public abstract class Pessoa
    {
        [Key]
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool Sexo { get; set; }
        public DateTime DataNascimento { get; set; }
    }
}
