﻿using APISGO;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class IdentityInitializer
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;


        public IdentityInitializer(
            ApplicationDbContext context,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public void Initialize()
        {
            if (_context.Database.EnsureCreated())
            {
                if (!_roleManager.RoleExistsAsync(Roles.ROLE_API_FUNCIONARIO).Result)
                {
                    var resultado = _roleManager.CreateAsync(
                        new IdentityRole(Roles.ROLE_API_FUNCIONARIO)).Result;
                    if (!resultado.Succeeded)
                    {
                        throw new Exception(
                            $"Erro durante a criação da role {Roles.ROLE_API_FUNCIONARIO}.");
                    }
                }

                CreateUser(
                    new ApplicationUser()
                    {
                        UserName = "admin_apialturas",
                        Email = "admin-apialturas@teste.com.br",
                        EmailConfirmed = true
                    }, "AdminAPIAlturas01!", Roles.ROLE_API_FUNCIONARIO);

                CreateUser(
                    new ApplicationUser()
                    {
                        UserName = "usrinvalido_apialturas",
                        Email = "usrinvalido-apialturas@teste.com.br",
                        EmailConfirmed = true
                    }, "UsrInvAPIAlturas01!");
            }
        }

        public void CreateUser(
            ApplicationUser user,
            string password,
            string initialRole = null)
        {
            if (_userManager.FindByNameAsync(user.UserName).Result == null)
            {
                var resultado = _userManager
                    .CreateAsync(user, password).Result;
                if (resultado.Succeeded &&
                    !String.IsNullOrWhiteSpace(initialRole))
                {
                    _userManager.AddToRoleAsync(user, initialRole).Wait();
                }
            }
        }
        public void  CreateRoleAdministrator()
        {
            Task<bool> roleexists = _roleManager.RoleExistsAsync(Roles.ROLE_API_ADMINISTRATOR);
            roleexists.Wait();
            if(roleexists.Result == false)
            {
                Task<IdentityResult> t2 = _roleManager.CreateAsync(new IdentityRole(Roles.ROLE_API_ADMINISTRATOR));
                t2.Wait();
            }
            ApplicationUser applicationUser = new ApplicationUser();
            applicationUser.Email = "administratorsgo@hotmail.com";
            applicationUser.EmailConfirmed = true;
            applicationUser.UserName = applicationUser.Email;
            var userexists = _userManager.FindByEmailAsync(applicationUser.Email);
            userexists.Wait();
            if(userexists.Result == null)
            {
                var resultregisteraccount = _userManager.CreateAsync(applicationUser, "Teste@123").Result;
                if (resultregisteraccount.Succeeded)
                {
                    var toatrributerole = _userManager.AddToRoleAsync(applicationUser, Roles.ROLE_API_ADMINISTRATOR);
                    toatrributerole.Wait();
                }
            }
            }
        public void CreateFuncionario()
        {
            Task<bool> roleexists = _roleManager.RoleExistsAsync(Roles.ROLE_API_FUNCIONARIO);
            roleexists.Wait();
            if (roleexists.Result == false)
            {
                Task<IdentityResult> t2 = _roleManager.CreateAsync(new IdentityRole(Roles.ROLE_API_FUNCIONARIO));
                t2.Wait();
            }
            ApplicationUser applicationUser = new ApplicationUser();
            applicationUser.Email = "igor_memories@hotmail.com";
            applicationUser.EmailConfirmed = true;
            applicationUser.UserName = applicationUser.Email;
            var userexists = _userManager.FindByEmailAsync(applicationUser.Email);
            userexists.Wait();
            if (userexists.Result == null)
            {
                var resultregisteraccount = _userManager.CreateAsync(applicationUser, "Igor@123").Result;
                if (resultregisteraccount.Succeeded)
                {
                    var toatrributerole = _userManager.AddToRoleAsync(applicationUser, Roles.ROLE_API_FUNCIONARIO);
                    toatrributerole.Wait();
                }
            }
        }
          
        }
    }
