﻿using SGO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class TaxaObitoSexo : TaxaObito, ITaxaObitoSexo
    {
        public enum Sexo
        {
            Masculino = 1,
            Feminino = 0
        }
        public ICollection<TaxaObitoSexo> GetDadosObitosSexo(Causa causa)
        {
            IList<TaxaObitoSexo> listataxaObitoSexos = new List<TaxaObitoSexo>();
            TaxaObitoSexo taxaObitoSexoMasculino = new TaxaObitoSexo();
            taxaObitoSexoMasculino.QuantidadeDeObitos = causa.Obitos.Where(x => x.Paciente.Sexo = Convert.ToBoolean(Sexo.Masculino)).Count();
            listataxaObitoSexos.Add(taxaObitoSexoMasculino);
            TaxaObitoSexo taxaObitoSexoFeminino = new TaxaObitoSexo();
            taxaObitoSexoFeminino.QuantidadeDeObitos = causa.Obitos.Where(x => x.Paciente.Sexo = Convert.ToBoolean(Sexo.Feminino)).Count();
            listataxaObitoSexos.Add(taxaObitoSexoFeminino);
            return listataxaObitoSexos;
        }
    }
}
