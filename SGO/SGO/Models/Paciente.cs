﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class Paciente:Pessoa
    {
        public string NumeroRegistro { get; set;}
        public string Endereco { get; set; }
        public int ObitoId { get; set; }
        public Obito Obito { get; set; }
    }
}
