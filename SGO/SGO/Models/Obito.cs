﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class Obito
    { 
        [Key]
        public int NumeroOrdem { get; set; }
        public string NumeroCaixa { get; set; }
        public Paciente Paciente { get; set; }
        public DateTime DataFalecimento { get; set; }
        public string Numeracao { get; set; }
        public Medico Medico { get; set; }
        public Clinica Clinica { get; set; }
        public Causa Causa { get; set; }
        public ApplicationUser Funcionario { get; set; }
    }
}
