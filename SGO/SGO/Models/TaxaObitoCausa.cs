﻿using SGO.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class TaxaObitoCausa:TaxaObito, ITaxaObitoCausa
    {
        public Causa Causa { get; set; }
        public TaxaObitoCausa GetDadosObitosCausa(IList<Obito> obitos)
        {
            QuantidadeDeObitos = obitos.Count();
            return this;
        }
        public IList<TaxaObitoCausa> GetDadosObitosCausaFrequente(IList<Causa> causas, int ano)
        {
            IList<TaxaObitoCausa> listataxaObitoCausas = new List<TaxaObitoCausa>();
            foreach (var causa in causas)
            {
                TaxaObitoCausa taxaObitoCausa = new TaxaObitoCausa();
                taxaObitoCausa.Causa = causa;
                taxaObitoCausa.QuantidadeDeObitos = causa.Obitos.Count();
                listataxaObitoCausas.Add(taxaObitoCausa);
            }
            return listataxaObitoCausas;
        }
    }
}
