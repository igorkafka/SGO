﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class ApplicationUser: IdentityUser,IPessoa
    {
        public string Endereco { get; set; }
        public string Matricula { get; set; }
        public virtual ICollection<Obito> Obitos { get; set; }
        public bool Sexo { get; set; }
        public DateTime DataNascimento { get; set; }
    }
}
