﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SGO.Models
{
    public class Especialidade
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public virtual ICollection<Medico> Medicos { get; set;}
    }
}
