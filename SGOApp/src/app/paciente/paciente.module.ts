import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PacienteRoutingModule } from './paciente-routing.module';
import { PacienteComponent } from './paciente.component';
import { PacienteListaComponent } from './paciente-lista/paciente-lista.component';
import { PacienteCadastroComponent } from './paciente-cadastro/paciente-cadastro.component';


@NgModule({
  imports: [
    CommonModule,
    PacienteRoutingModule
  ],
  declarations: [PacienteComponent, PacienteListaComponent, PacienteCadastroComponent]
})
export class PacienteModule { }
