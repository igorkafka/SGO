import { PacienteListaComponent } from './paciente-lista/paciente-lista.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PacienteComponent } from './paciente.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            { path: 'paciente', component: PacienteComponent, children: [
                {path: '', component: PacienteListaComponent }
            ] }
        ])
    ],
    exports: [RouterModule]
})
export class PacienteRoutingModule {
}
