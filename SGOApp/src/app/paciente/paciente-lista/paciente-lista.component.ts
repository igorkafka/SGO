import { Component, OnInit } from '@angular/core';
import { Paciente } from './../../Models/paciente';
@Component({
  selector: 'app-paciente-lista',
  templateUrl: './paciente-lista.component.html',
  styleUrls: ['./paciente-lista.component.css']
})
export class PacienteListaComponent implements OnInit {
  pacientes: Paciente[];
  constructor() {
    const p = new Paciente();
    this.pacientes = [p];
  }

  ngOnInit() {
  }

}
