import { Observable } from 'rxjs/observable';
import { TaxaObitoMes } from './../../Models/TaxaObitoMes';
import { LoaderService } from './../../Shared/loader.service';
import { AuthService } from './../../core/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TaxaMesService {

  constructor(private http: HttpClient, private auth: AuthService, private loader: LoaderService) { }
  GetTaxaMes(ano: number): Observable<TaxaObitoMes[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    return this.http.get<TaxaObitoMes[]>('/api/taxaobito/listartaxapormeses?ano=' + ano, {headers: headers});
  }
}
