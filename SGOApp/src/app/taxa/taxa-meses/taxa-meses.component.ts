import { Component, OnInit } from '@angular/core';
import { TaxaObitoMes } from 'src/app/Models/TaxaObitoMes';
import { NgForm } from '@angular/forms';
import { TaxaMesService } from './taxa-mes.service';
import { Anos } from 'src/app/Shared/constantes';

@Component({
  selector: 'app-taxa-meses',
  templateUrl: './taxa-meses.component.html',
  styleUrls: ['./taxa-meses.component.css']
})
export class TaxaMesesComponent implements OnInit {
  ListaTaxaObito: TaxaObitoMes[] = new Array<TaxaObitoMes>();
  ngForm: NgForm;
  anos: string[] = Anos.GetAnos();
anoselecionado = '2018';
public lineChartLabels: Array<any> = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Março', 'Junho', 'Julho',
'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
public lineChartData: Array<any> = [
  {data: [], label: 'Óbitos'}
];
public lineChartOptions: any = {
  responsive: true
};

public lineChartColors: Array<any> = [
  { // grey
    backgroundColor: 'rgba(148,159,177,0.2)',
    borderColor: 'rgba(148,159,177,1)',
    pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  },
  { // dark grey
    backgroundColor: 'rgba(77,83,96,0.2)',
    borderColor: 'rgba(77,83,96,1)',
    pointBackgroundColor: 'rgba(77,83,96,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(77,83,96,1)'
  },
  { // grey
    backgroundColor: 'rgba(148,159,177,0.2)',
    borderColor: 'rgba(148,159,177,1)',
    pointBackgroundColor: 'rgba(148,159,177,1)',
    pointBorderColor: '#fff',
    pointHoverBackgroundColor: '#fff',
    pointHoverBorderColor: 'rgba(148,159,177,0.8)'
  }
];
public lineChartLegend: boolean = true;
public lineChartType: string = 'line';
  constructor(private taxamesservice: TaxaMesService) {
   }
  ngOnInit() {
  }
  public randomize(): void {
    const _lineChartData: Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < this.lineChartData.length; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < this.lineChartData[i].data.length; j++) {
        _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
      }
    }
    this.lineChartData = _lineChartData;
  }

  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
  SetQuantidadeDeObitos() {
     const quantidadesdeobitos = new Array<number>();
     this.ListaTaxaObito.forEach((element, index) => {
       quantidadesdeobitos.push(element.quantidadeDeObitos);
     });
     console.log(quantidadesdeobitos);
     console.log(this.ListaTaxaObito);
     this.lineChartData = [{data: quantidadesdeobitos, label: 'Óbitos'}];
  }
  onSubmit() {
    this.ListaTaxaObito = new Array<TaxaObitoMes>();
    this.taxamesservice.GetTaxaMes(Number.parseInt((this.anoselecionado))).subscribe(data => {
      data.forEach(element => {
        this.ListaTaxaObito.push(element);
      });
    }, error => {
      console.log(error);
    }, () => {
      this.SetQuantidadeDeObitos();
    });
  }
}
