import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxaMesesComponent } from './taxa-meses.component';

describe('TaxaMesesComponent', () => {
  let component: TaxaMesesComponent;
  let fixture: ComponentFixture<TaxaMesesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxaMesesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaMesesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
