import { TestBed, inject } from '@angular/core/testing';

import { TaxaMesService } from './taxa-mes.service';

describe('TaxaMesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaxaMesService]
    });
  });

  it('should be created', inject([TaxaMesService], (service: TaxaMesService) => {
    expect(service).toBeTruthy();
  }));
});
