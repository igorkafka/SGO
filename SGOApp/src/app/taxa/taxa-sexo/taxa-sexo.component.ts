import { Component, OnInit, ViewChild } from '@angular/core';
import {BaseChartDirective} from 'ng2-charts/ng2-charts';
import { TaxaService } from '../taxa.service';

@Component({
  selector: 'app-taxa-sexo',
  templateUrl: './taxa-sexo.component.html',
  styleUrls: ['./taxa-sexo.component.css']
})
export class TaxaSexoComponent implements OnInit {
  sexo: boolean = true;
  @ViewChild(BaseChartDirective) public chart: BaseChartDirective;
  public pieChartLabels: string[] = [];
  public pieChartData: number[] = [];
  public pieChartType: string = 'pie';
  labels: string[] = new Array<string>();
  ChartData: number[] = new Array<number>();
  constructor(private taxaservice: TaxaService) { }

  ngOnInit() {
  }
  onSubmit() {
    this.taxaservice.GetDateCausaGenero(this.sexo).subscribe(data => {
      this.labels = [];
      this.ChartData = [];
      this.pieChartLabels = [];
      this.pieChartData = [];
       // tslint:disable-next-line:no-shadowed-variable
       data.forEach( element => {
         this.ChartData.push(element.quantidadeDeObitos);
         this.labels.push(element.causa.nome);
       });
    }, error => {
      console.log(error);
    }, () => {
      this.pieChartData = this.ChartData;
      setTimeout(() => {
        if (this.chart && this.chart.chart && this.chart.chart.config) {
            this.chart.chart.config.data.labels = this.labels;
            this.pieChartLabels = this.labels;
            this.chart.labels = this.labels;
            this.chart.chart.data.labels = this.labels;
            this.chart.chart.update();
        }
    });
    });
  }
  public chartClicked(e: any): void {
   console.log(e);
 }
 public chartHovered(e: any): void {
   console.log(e);
 }
}
