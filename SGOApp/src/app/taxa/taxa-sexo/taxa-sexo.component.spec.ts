import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxaSexoComponent } from './taxa-sexo.component';

describe('TaxaSexoComponent', () => {
  let component: TaxaSexoComponent;
  let fixture: ComponentFixture<TaxaSexoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxaSexoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaSexoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
