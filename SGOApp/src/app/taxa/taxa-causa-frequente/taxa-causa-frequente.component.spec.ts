import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxaCausaFrequenteComponent } from './taxa-causa-frequente.component';

describe('TaxaCausaFrequenteComponent', () => {
  let component: TaxaCausaFrequenteComponent;
  let fixture: ComponentFixture<TaxaCausaFrequenteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxaCausaFrequenteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaCausaFrequenteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
