import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Anos } from 'src/app/Shared/constantes';
import { TaxaService } from '../taxa.service';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-taxa-causa-frequente',
  templateUrl: './taxa-causa-frequente.component.html',
  styleUrls: ['./taxa-causa-frequente.component.css']
})
export class TaxaCausaFrequenteComponent implements OnInit {
  ngForm: NgForm;
  anos: string[] = Anos.GetAnos();
anoselecionado = '2018';
public pieChartLabels: string[] = [];
public pieChartData: number[] = [];
public pieChartType: string = 'pie';
labels: string[] = new Array<string>();
ChartData: number[] = new Array<number>();
@ViewChild(BaseChartDirective) public chart: BaseChartDirective;
  constructor(private taxaservice: TaxaService) { }
  ngOnInit() {
  }
  onSubmit() {
    this.taxaservice.GetDateCausaFrequente(parseInt(this.anoselecionado, 10)).subscribe(data => {
      this.ChartData = [];
      this.labels = [];
      data.forEach(element => {
        this.labels.push(element.causa.nome);
        this.ChartData.push(element.quantidadeDeObitos);
      });
    }, error => {
      console.log(error);
    }, () => {
      this.pieChartData = this.ChartData;
      setTimeout(() => {
        if (this.chart && this.chart.chart && this.chart.chart.config) {
            this.chart.chart.config.data.labels = this.labels;
            this.pieChartLabels = this.labels;
            this.chart.labels = this.labels;
            this.chart.chart.data.labels = this.labels;
            this.chart.chart.update();
        }
    });
    });
  }
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
