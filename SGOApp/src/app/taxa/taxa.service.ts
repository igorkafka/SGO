import { TaxaObitoClinica } from './../Models/TaxaObitoClinica';
import { Observable, Subject } from 'rxjs';
import { TaxaObitoCausa } from './../Models/taxaobitocausa';
import { AuthService } from 'src/app/core/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Causa } from '../Models/causa';
import { TaxaPesquisaCausaComponent } from './taxa-pesquisa-causa/taxa-pesquisa-causa.component';

@Injectable({
  providedIn: 'root'
})
export class TaxaService {
  taxaobitocausasubject = new Subject<TaxaObitoCausa>();
  constructor(private http: HttpClient, private auth: AuthService, private modalService: NgbModal) { }
  AddCausa(causa: Causa): Observable<TaxaObitoCausa> {
    const obs = new Observable();
      const headers = new HttpHeaders({
        'Authorization': 'Bearer ' + this.auth.token.accessToken
      });
      return  this.http.get<TaxaObitoCausa>('/api/taxaobito/taxacausa?idcausa=' + causa.id ,
        {headers: headers});
      }
  GetDateTaxaClinica(ano: number): Observable<TaxaObitoClinica[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
   return this.http.get<TaxaObitoClinica[]>('/api/taxaobito/taxaclinica?ano=' + ano
   , {headers: headers });
  }
  GetDateCausaFrequente(ano: number): Observable<TaxaObitoCausa[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
   return this.http.get<TaxaObitoCausa[]>('/api/taxaobito/taxacausafrequente?ano=' + ano
   , {headers: headers });
  }
  GetDateCausaGenero(genero: boolean): Observable<TaxaObitoCausa[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
   return this.http.get<TaxaObitoCausa[]>('/api/taxaobito/taxagenero?genero=' + genero
   , {headers: headers });
  }
}
