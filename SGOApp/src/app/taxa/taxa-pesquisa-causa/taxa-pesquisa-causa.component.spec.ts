import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxaPesquisaCausaComponent } from './taxa-pesquisa-causa.component';

describe('TaxaPesquisaCausaComponent', () => {
  let component: TaxaPesquisaCausaComponent;
  let fixture: ComponentFixture<TaxaPesquisaCausaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxaPesquisaCausaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaPesquisaCausaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
