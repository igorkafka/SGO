import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef, Input } from '@angular/core';
import { Causa } from 'src/app/Models/causa';
import { CausaService } from 'src/app/causa/causa.service';
import { CausaListaService } from 'src/app/causa/causa-lista.service';
import { TaxaService } from '../taxa.service';
declare var jQuery: any;
@Component({
  selector: 'app-taxa-pesquisa-causa',
  templateUrl: './taxa-pesquisa-causa.component.html',
  styleUrls: ['./taxa-pesquisa-causa.component.css']
})
export class TaxaPesquisaCausaComponent implements OnInit {
 @Input() isHidden: boolean = true;
  causas: Causa[];
  @Output() causaadicionada = new EventEmitter();
  @ViewChild('campoinput') campoinput: ElementRef;
  pagina: number = 1;
  isCreateOrEditObito;
  constructor(private causaservice: CausaService, private causalista: CausaListaService,
    private taxaservice: TaxaService) { }

  ngOnInit() {
    this.causalista.causalistchanged.subscribe(data => {
      this.causas = data;
    });
  }
  Pesquisar() {
    this.pagina = 1;
    this.causaservice.ListarCausas(this.campoinput.nativeElement.value, this.pagina);
  }
  onScroll() {
    this.pagina++;
    this.causaservice.ListarCausas(this.campoinput.nativeElement.value, this.pagina);
  }
  AdicionarCausa(causa: Causa) {
    this.causas = null;
    this.causaadicionada.emit(causa);
    jQuery('#myModal').modal('hide');
  }
}
