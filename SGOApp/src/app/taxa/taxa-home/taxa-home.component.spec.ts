import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxaHomeComponent } from './taxa-home.component';

describe('TaxaHomeComponent', () => {
  let component: TaxaHomeComponent;
  let fixture: ComponentFixture<TaxaHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxaHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
