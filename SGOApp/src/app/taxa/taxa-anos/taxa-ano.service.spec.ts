import { TestBed, inject } from '@angular/core/testing';

import { TaxaAnoService } from './taxa-ano.service';

describe('TaxaAnoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaxaAnoService]
    });
  });

  it('should be created', inject([TaxaAnoService], (service: TaxaAnoService) => {
    expect(service).toBeTruthy();
  }));
});
