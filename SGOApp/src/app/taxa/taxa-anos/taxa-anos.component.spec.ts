import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxaAnosComponent } from './taxa-anos.component';

describe('TaxaAnosComponent', () => {
  let component: TaxaAnosComponent;
  let fixture: ComponentFixture<TaxaAnosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxaAnosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaAnosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
