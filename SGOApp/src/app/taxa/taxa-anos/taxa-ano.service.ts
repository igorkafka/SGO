import { TaxaObitoAno } from './../../Models/TaxaObitoAno';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from 'src/app/core/auth.service';
import { LoaderService } from 'src/app/Shared/loader.service';
import { Observable } from 'rxjs';
import { TaxaObitoMes } from 'src/app/Models/TaxaObitoMes';

@Injectable({
  providedIn: 'root'
})
export class TaxaAnoService {

  constructor(private http: HttpClient, private auth: AuthService, private loader: LoaderService) { }
  GetTaxaAno(anoinicial: number, anofinal: number): Observable<TaxaObitoAno[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    return this.http.get<TaxaObitoAno[]>('/api/taxaobito/listartaxaporanos?anoinicial=' +
     anoinicial + '&anofinal=' + anofinal, {headers: headers});
  }
}
