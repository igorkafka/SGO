import { Anos } from './../../Shared/constantes';
import { TaxaObitoAno } from './../../Models/TaxaObitoAno';
import { Component, OnInit } from '@angular/core';
import { TaxaAnoService } from './taxa-ano.service';

@Component({
  selector: 'app-taxa-anos',
  templateUrl: './taxa-anos.component.html',
  styleUrls: ['./taxa-anos.component.css']
})
export class TaxaAnosComponent implements OnInit {
  anoinicial: string = '2000';
  anofinal: string = '2019';
  ListaTaxaObito: TaxaObitoAno[] = new Array<TaxaObitoAno>();
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
    {data: [], label: 'Obitos'},
  ];

  anos: string[] = Anos.GetAnos();
  constructor(private taxaobitoanoservice: TaxaAnoService) { }
// events
public chartClicked(e: any): void {
  console.log(e);
}

public chartHovered(e: any): void {
  console.log(e);
}

public randomize(): void {
  // Only Change 3 values
  const data = [
    Math.round(Math.random() * 100),
    59,
    80,
    (Math.random() * 100),
    56,
    (Math.random() * 100),
    40];
  const clone = JSON.parse(JSON.stringify(this.barChartData));
  clone[0].data = data;
  this.barChartData = clone;
  /**
   * (My guess), for Angular to recognize the change in the dataset
   * it has to change the dataset variable directly,
   * so one way around it, is to clone the data, change it and then
   * assign it;
   */
}
  ngOnInit() {
  }
  SetQuantidadeDeObitos() {
    const quantidadesdeobitos = new Array<number>();
    this.barChartLabels = [];
    console.log(this.ListaTaxaObito);
    this.ListaTaxaObito.forEach((element, index) => {
      quantidadesdeobitos.push(element.quantidadeDeObitos);
      this.barChartLabels.push((element.ano).toString());
    });
    this.barChartData[0] = {data: quantidadesdeobitos, label: 'Obitos'};
    console.log(this.barChartData);
 }
 onSubmit() {
  this.ListaTaxaObito = new Array<TaxaObitoAno>();
   this.taxaobitoanoservice.GetTaxaAno(Number.parseInt(this.anoinicial), Number.parseInt(this.anofinal)).subscribe(data => {
         data.forEach( element => {
          this.ListaTaxaObito.push(element);
         });
   }, error => console.log(error), () => {
     this.SetQuantidadeDeObitos();
   });
 }

}
