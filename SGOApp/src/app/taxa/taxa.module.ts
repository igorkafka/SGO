import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts';
import { SelectModule } from 'ng2-select';
import { CommonModule } from '@angular/common';
import { TaxaComponent } from './taxa/taxa.component';
import { TaxaAnosComponent } from './taxa-anos/taxa-anos.component';
import { TaxaMesesComponent } from './taxa-meses/taxa-meses.component';
import { TaxaRoutingModule } from './taxa-routing-module';
import { TaxaHomeComponent } from './taxa-home/taxa-home.component';
import { TaxaCausaComponent } from './taxa-causa/taxa-causa.component';
import { ObitoModule } from '../obito/obito.module';
import { TaxaPesquisaCausaComponent } from './taxa-pesquisa-causa/taxa-pesquisa-causa.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { TaxaClinicaComponent } from './taxa-clinica/taxa-clinica.component';
import { TaxaCausaFrequenteComponent } from './taxa-causa-frequente/taxa-causa-frequente.component';
import { TaxaSexoComponent } from './taxa-sexo/taxa-sexo.component';

@NgModule({
  imports: [
    CommonModule,
    TaxaRoutingModule,
    ChartsModule,
    FormsModule,
    SelectModule,
    NgbModalModule,
    ObitoModule,
    InfiniteScrollModule
  ],
  declarations: [TaxaComponent,
    TaxaAnosComponent, TaxaMesesComponent,
     TaxaHomeComponent, TaxaCausaComponent,
     TaxaPesquisaCausaComponent, TaxaClinicaComponent,
      TaxaCausaFrequenteComponent, TaxaSexoComponent]
})
export class TaxaModule { }
