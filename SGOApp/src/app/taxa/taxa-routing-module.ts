import { TaxaSexoComponent } from './taxa-sexo/taxa-sexo.component';
import { TaxaCausaFrequenteComponent } from './taxa-causa-frequente/taxa-causa-frequente.component';
import { TaxaClinicaComponent } from './taxa-clinica/taxa-clinica.component';
import { ObitoPesquisaCausaComponent } from './../obito/obito-cadastrar/obito-pesquisa-causa/obito-pesquisa-causa.component';
import { TaxaCausaComponent } from './taxa-causa/taxa-causa.component';
import { TaxaHomeComponent } from './taxa-home/taxa-home.component';
import { TaxaMesesComponent } from './taxa-meses/taxa-meses.component';
import { TaxaAnosComponent } from './taxa-anos/taxa-anos.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TaxaComponent } from './taxa/taxa.component';
import { TaxaPesquisaCausaComponent } from './taxa-pesquisa-causa/taxa-pesquisa-causa.component';
@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'taxa', component: TaxaComponent, children: [
        {path: '', component: TaxaHomeComponent},
        {path: 'taxa-anos', component: TaxaAnosComponent},
        {path: 'taxa-meses', component: TaxaMesesComponent},
        {path: 'taxa-causa', component: TaxaCausaComponent},
        {path: 'taxa-pesquisa-causa', component: TaxaPesquisaCausaComponent},
        {path: 'taxa-clinica', component: TaxaClinicaComponent},
        {path: 'taxa-causa-frequente', component: TaxaCausaFrequenteComponent},
        {path: 'taxa-sexo', component: TaxaSexoComponent}
      ]}
    ])
  ],
  exports: [RouterModule]
})
export class TaxaRoutingModule {

}
