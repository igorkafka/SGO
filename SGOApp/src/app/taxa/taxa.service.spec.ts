import { TestBed, inject } from '@angular/core/testing';

import { TaxaService } from './taxa.service';

describe('TaxaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TaxaService]
    });
  });

  it('should be created', inject([TaxaService], (service: TaxaService) => {
    expect(service).toBeTruthy();
  }));
});
