import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxaClinicaComponent } from './taxa-clinica.component';

describe('TaxaClinicaComponent', () => {
  let component: TaxaClinicaComponent;
  let fixture: ComponentFixture<TaxaClinicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxaClinicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaClinicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
