import { Subscription } from 'rxjs';
import { TaxaService } from './../taxa.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Anos } from 'src/app/Shared/constantes';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';

@Component({
  selector: 'app-taxa-clinica',
  templateUrl: './taxa-clinica.component.html',
  styleUrls: ['./taxa-clinica.component.css']
})
export class TaxaClinicaComponent implements OnInit, OnDestroy {

  ngForm: NgForm;
  subscription: Subscription;
  anos: string[] = Anos.GetAnos();
anoselecionado = '2018';
public polarAreaChartLabels: string[] = [];
  public polarAreaChartData: number[] = [];
  public polarAreaLegend: boolean = true;
  public polarAreaChartType: string = 'polarArea';
  labels: string[] = new Array<string>();
  ChartData: number[] = new Array<number>();
  @ViewChild(BaseChartDirective) public chart: BaseChartDirective;
  constructor(private taxaservice: TaxaService) { }

  ngOnInit() {
  }
  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  onSubmit() {
  this.subscription = this.taxaservice.GetDateTaxaClinica(parseInt(this.anoselecionado, 10)).subscribe(data => {
      this.ChartData = [];
      this.labels = [];
      data.forEach(element => {
        this.labels.push(element.clinica.nome);
        this.ChartData.push(element.quantidadeDeObitos);
      });
    }, error => {
      console.log(error);
    }, () => {
      this.polarAreaChartData = this.ChartData;
      setTimeout(() => {
        if (this.chart && this.chart.chart && this.chart.chart.config) {
            this.chart.chart.config.data.labels = this.labels;
            this.polarAreaChartLabels = this.labels;
            this.chart.labels = this.labels;
            this.chart.chart.data.labels = this.labels;
            this.chart.chart.update();
        }
    });
  });
}
}
