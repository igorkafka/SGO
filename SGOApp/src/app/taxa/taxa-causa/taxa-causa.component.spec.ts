import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxaCausaComponent } from './taxa-causa.component';

describe('TaxaCausaComponent', () => {
  let component: TaxaCausaComponent;
  let fixture: ComponentFixture<TaxaCausaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxaCausaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxaCausaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
