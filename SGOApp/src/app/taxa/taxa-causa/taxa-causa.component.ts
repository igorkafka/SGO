import { element } from 'protractor';
import { TaxaService } from './../taxa.service';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TaxaPesquisaCausaComponent } from '../taxa-pesquisa-causa/taxa-pesquisa-causa.component';
import { Causa } from 'src/app/Models/causa';
import { TaxaObitoCausa } from 'src/app/Models/taxaobitocausa';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { Subscription } from 'rxjs';
import * as $ from 'jquery';
@Component({
  selector: 'app-taxa-causa',
  templateUrl: './taxa-causa.component.html',
  styleUrls: ['./taxa-causa.component.css']
})
export class TaxaCausaComponent implements OnInit, OnDestroy {
  public items: Array<string> = ['Amsterdam', 'Antwerp', 'Athens', 'Barcelona',
  'Berlin', 'Birmingham', 'Bradford', 'Bremen', 'Brussels', 'Bucharest',
  'Budapest', 'Cologne', 'Copenhagen', 'Dortmund', 'Dresden', 'Dublin', 'Düsseldorf',
  'Essen', 'Frankfurt', 'Genoa', 'Glasgow', 'Gothenburg', 'Hamburg', 'Hannover',
  'Helsinki', 'Leeds', 'Leipzig', 'Lisbon', 'Łódź', 'London', 'Kraków', 'Madrid',
  'Málaga', 'Manchester', 'Marseille', 'Milan', 'Munich', 'Naples', 'Palermo',
  'Paris', 'Poznań', 'Prague', 'Riga', 'Rome', 'Rotterdam', 'Seville', 'Sheffield',
  'Sofia', 'Stockholm', 'Stuttgart', 'The Hague', 'Turin', 'Valencia', 'Vienna',
  'Vilnius', 'Warsaw', 'Wrocław', 'Zagreb', 'Zaragoza'];

 public value: any = ['Athens'];
  @ViewChild(BaseChartDirective) public chart: BaseChartDirective;
  public pieChartLabels: string[] = [];
  public pieChartData: number[] = [];
  public pieChartType: string = 'pie';
  labels: string[] = new Array<string>();
  subscription: Subscription;
  ChartData: number[] = new Array<number>();
  listadetaxaobitocausa: TaxaObitoCausa[] = new Array<TaxaObitoCausa>();
  constructor(private modalService: NgbModal, private taxaservice: TaxaService) {
  }
  ngOnInit() {
    $(document).ready(function() {
      $('#btnadicionarcausa').click(function() {
      });
  });
    this.subscription = this.taxaservice.taxaobitocausasubject.subscribe(data => {
      this.chart.getChartBuilder(this.chart.ctx);
      this.ChartData.push(data.quantidadeDeObitos);
      this.labels.push(data.causa.nome);
      this.pieChartData = this.ChartData;
      setTimeout(() => {
        if (this.chart && this.chart.chart && this.chart.chart.config) {
            this.chart.chart.config.data.labels = this.labels;
            this.pieChartLabels = this.labels;
            this.chart.labels = this.labels;
            this.chart.chart.data.labels = this.labels;
            this.chart.chart.update();
        }
    });
  }, error => {
    console.log(error);
  }, () => {
    alert('oi');
  });
  }
  open() {
    const modalRef = this.modalService.open(TaxaPesquisaCausaComponent, {size: 'lg', windowClass: 'modal-adaptive'});
  }
   public chartClicked(e: any): void {
    console.log(e);
  }
  public chartHovered(e: any): void {
    console.log(e);
  }
  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
  public selected(value: any): void {
    console.log('Selected value is: ', value);
  }

  public removed(value: any): void {
    console.log('Removed value is: ', value);
  }

  public refreshValue(value: any): void {
    this.value = value;
  }
  // tslint:disable-next-line:member-ordering
  public chartColors: any[] = [
    {
      backgroundColor: ['#FF7360', '#6FC8CE', '#C71585', '#FF0000', '#F0E68C', '#808000', '#556B2F',
    '#00008B']
    }];
  AdicionarCausa(causa: Causa) {
    this.taxaservice.AddCausa(causa).subscribe(data => {
      this.ChartData.push(data.quantidadeDeObitos);
      this.labels.push(data.causa.nome);
      this.pieChartData = this.ChartData;
    }, error => {
      console.log(error);
    }, () => {
      setTimeout(() => {
        if (this.chart && this.chart.chart && this.chart.chart.config) {
            this.chart.chart.config.data.labels = this.labels;
            this.pieChartLabels = this.labels;
            this.chart.labels = this.labels;
            this.chart.chart.data.labels = this.labels;
            this.chart.chart.update();
        }
    });
    });
  }
  public itemsToString(value: Array<any> = []): string {
    return value
      .map((item: any) => {
        return item.text;
      }).join(',');
  }
}
