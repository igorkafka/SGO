import { CaixaRoutingModule } from './caixa-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CaixaComponent } from './caixa.component';
import { CaixaListaComponent } from './caixa-lista/caixa-lista.component';

@NgModule({
  imports: [
    CommonModule,
    CaixaRoutingModule
  ],
  declarations: [CaixaComponent, CaixaListaComponent]
})
export class CaixaModule { }
