import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CaixaComponent } from './caixa.component';
import { AuthGuardAdminService } from '../core/AuthGuardAdminService';
import { CaixaListaComponent } from './caixa-lista/caixa-lista.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'caixa', component: CaixaComponent, canActivate: [AuthGuardAdminService], children: [
        {path: '', component: CaixaListaComponent}
      ]  }
    ])
  ],
  exports: [RouterModule]
})
export class CaixaRoutingModule {

}
