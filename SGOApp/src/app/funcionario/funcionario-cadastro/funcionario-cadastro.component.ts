import { RegisterUser } from 'src/app/Models/RegisterUser.model';
import { FormGroup, FormControl, Validators, AbstractControl, NgForm } from '@angular/forms';
import { Component, OnInit, OnDestroy, ViewEncapsulation, Input } from '@angular/core';
import { RegexExpression } from 'src/app/Shared/RegexExpression';
import { AuthService } from '../../core/auth.service';
import { NgbDatepickerConfig, NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { LoaderService } from '../../Shared/loader.service';

@Component({
  selector: 'app-funcionario-cadastro',
  templateUrl: './funcionario-cadastro.component.html',
  styleUrls: ['./funcionario-cadastro.component.css'],
  encapsulation: ViewEncapsulation.None,


})
export class FuncionarioCadastroComponent implements OnInit, OnDestroy {
  funcionariocadastroform: FormGroup;
  closeResult: string;
  isLoading: boolean = false;
  model;
  constructor(private auth: AuthService, private loaderservice: LoaderService,
     public activeModal: NgbActiveModal, private modalService: NgbModal,
    config: NgbDatepickerConfig) {
    config.minDate = {year: 1900, month: 1, day: 1};
    config.maxDate = {year: 2099, month: 12, day: 31};
    this.initform();
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  ngOnInit() {

  }
  onSubmit(form) {
    if (form.valid) {
      this.loaderservice.isLoading = true;
      const registeruser = new RegisterUser();
      registeruser.userEmail = form.value.email;
      registeruser.userName = form.value.nome;
      registeruser.Perfil = form.value.perfil;
      registeruser.Password = form.value.password;
      registeruser.email = form.value.email;
      registeruser.nome = form.value.nome;
      registeruser.endereco = form.value.endereco;
      registeruser.matricula = form.value.matricula;
      registeruser.dataNascimento =
       new   Date(form.value.datanascimento.year, form.value.datanascimento.month - 1,
         form.value.datanascimento.day);
      console.log(registeruser);
      if (form.value.sexo === 'masculino') {
        registeruser.sexo = true;
      } else {
        registeruser.sexo = false;
      }
      this.auth.RegisterAccount(registeruser).subscribe( data => {
        console.log(data);
      }, error => {
        console.log(error);
      }, () => {
        this.activeModal.close();
      });
    } else {
      this.funcionariocadastroform.controls['nome'].markAsTouched();
      this.funcionariocadastroform.controls['datanascimento'].markAsTouched();
      this.funcionariocadastroform.controls['endereco'].markAsTouched();
      this.funcionariocadastroform.controls['sexo'].markAsTouched();
      this.funcionariocadastroform.controls['matricula'].markAsTouched();
      this.funcionariocadastroform.controls['email'].markAsTouched();
      this.funcionariocadastroform.controls['confirmpassword'].markAsTouched();
      this.funcionariocadastroform.controls['password'].markAsTouched();

    }
  }
  passwordConfirming(c: AbstractControl): any {
    if (!c.parent || !c) { return; }
    const pwd = c.parent.get('password');
    const cpwd = c.parent.get('confirmpassword');
    if (!pwd || !cpwd) { return ; }
    if (pwd.value !== cpwd.value) {
        return { invalid: true };
    }
  }
  initform() {
    this.funcionariocadastroform =  new FormGroup({
      'nome': new FormControl('', [Validators.required]),
      'datanascimento': new FormControl(Date.now, Validators.required),
      'endereco': new FormControl('', [Validators.required]),
      'perfil': new FormControl('funcionario'),
      'sexo': new FormControl('masculino', [Validators.required]),
      'matricula': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required, Validators.pattern(RegexExpression.EmailRegex())]),
      'confirmpassword': new FormControl('', [Validators.required, Validators.minLength(4),
         this.passwordConfirming] ),
      'password': new FormControl('', [Validators.required])
    });
  }
  ngOnDestroy(): void {
    this.loaderservice.isLoading = false;
  }
}
