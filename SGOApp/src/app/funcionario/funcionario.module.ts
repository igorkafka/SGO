import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbDatepickerModule, NgbPaginationModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FuncionarioComponent } from './funcionario.component';
import { FuncionarioListaComponent } from './funcionario-lista/funcionario-lista.component';
import { FuncioanrioRoutingModule } from './funcionario-routing.module';
import { FuncionarioCadastroComponent } from './funcionario-cadastro/funcionario-cadastro.component';
import { FuncionarioEditarComponent } from './funcionario-editar/funcionario-editar.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FuncioanrioRoutingModule,
    FormsModule,
    InfiniteScrollModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    NgbModalModule
  ],
  declarations: [FuncionarioComponent, FuncionarioListaComponent, FuncionarioCadastroComponent, FuncionarioEditarComponent],
  providers: []
})
export class FuncionarioModule { }
