import { FuncionarioEditarComponent } from './funcionario-editar/funcionario-editar.component';
import { AuthGuardAdminService } from './../core/AuthGuardAdminService';
import { FuncionarioCadastroComponent } from './funcionario-cadastro/funcionario-cadastro.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PacienteComponent } from '../paciente/paciente.component';
import { FuncionarioListaComponent } from './funcionario-lista/funcionario-lista.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'funcionario', component: PacienteComponent, canActivate: [AuthGuardAdminService], children: [
        {path: '', component: FuncionarioListaComponent},
        {path: 'cadastro', component: FuncionarioCadastroComponent},
        {path: 'editar', component: FuncionarioEditarComponent}
      ] }
    ])
  ],
  exports: [RouterModule]
})
export class FuncioanrioRoutingModule {

}
