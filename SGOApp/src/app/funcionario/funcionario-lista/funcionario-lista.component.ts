import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbDatepickerConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { LoaderService } from '../../Shared/loader.service';
import { FuncionarioCadastroComponent } from '../funcionario-cadastro/funcionario-cadastro.component';
import { FuncionarioEditarComponent } from '../funcionario-editar/funcionario-editar.component';
import { User } from '../../Models/User';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'app-funcionario-lista',
  templateUrl: './funcionario-lista.component.html',
  styleUrls: ['./funcionario-lista.component.css']
})
export class FuncionarioListaComponent implements OnInit {
  usuarios: User[];
  parametropesquisa: string = 'nome';
  subscription: Subscription;
  pagina: number = 1;
  showDateInput: boolean = false;
  @ViewChild('campoinput') campopesquisa: ElementRef;
  @ViewChild('campoinputdatanascimento') campoinputdatanascimento: ElementRef;
  constructor(private loaderservice:  LoaderService, private modalService: NgbModal,
    config: NgbDatepickerConfig, private auth: AuthService) {
   }
  ngOnInit() {
    this.initform();
  }
  initform() {
  }
  MudarParametroPesquisa(event) {
   this.pagina = 1;
   this.parametropesquisa = event.target.value;
   if (this.parametropesquisa !== 'datanascimento') {
    this.showDateInput = false;
   }  if (this.parametropesquisa === 'matricula') {
    this.campopesquisa.nativeElement.placeholder = 'Digite a Matrícula do Funcionário';
  } else if (this.parametropesquisa === 'endereco')  {
   this.campopesquisa.nativeElement.placeholder = 'Digite o Endereço do Funcionário';
  } else if (this.parametropesquisa === 'email') {
    this.campopesquisa.nativeElement.placeholder = 'Digite o Email do Funcionário';
  } else if (this.parametropesquisa === 'nome') {
    this.campopesquisa.nativeElement.placeholder = 'Digite o Nome do Funcionário';
  } else if (this.parametropesquisa === 'datanascimento') {
    this.showDateInput = true;
   }
   console.log(this.parametropesquisa);
  }
  Pesquisar() {
    this.pagina = 1;
    this.loaderservice.isLoading = true;
    if (this.subscription) {
      this.subscription.unsubscribe();
    } if (this.parametropesquisa === 'matricula') {
      this.auth.ListarPorMatricula(this.campopesquisa.nativeElement.value, this.pagina).subscribe(
        data => {
          this.usuarios = data;
        }, error => {
          console.log(error);
        }, () => {
          this.loaderservice.isLoading = false;
        }
      );
    } else if (this.parametropesquisa === 'endereco')  {
      this.auth.ListarPorEndereco(this.campopesquisa.nativeElement.value, this.pagina).subscribe(
        data => {
          console.log(data);
        this.usuarios = data;
      }, error => {
        console.log(error);
      }, () => {
        this.loaderservice.isLoading = false;
      });
    } else if (this.parametropesquisa === 'email') {
      this.auth.ListarPorEmail(this.campopesquisa.nativeElement.value, this.pagina).subscribe(
        data => {
          console.log(data);
        this.usuarios = data;
      }, error => {
        console.log(error);
      }, () => {
        this.loaderservice.isLoading = false;
      });
      } else if (this.parametropesquisa === 'nome') {
   this.subscription = this.auth.ListarUsuariosPorNome(this.campopesquisa.nativeElement.value, this.pagina)
     .subscribe(data => {
       this.usuarios = data;
       console.log(data);
     }, (error) => {
       console.log(error);
     }, () => {
      this.loaderservice.isLoading = false;
     });
    } else if (this.parametropesquisa === 'datanascimento') {
      console.log(this.campoinputdatanascimento.nativeElement.value);
      this.auth.ListarUsuariosPorData(this.campoinputdatanascimento.nativeElement.value, this.pagina).subscribe(data => {
      this.usuarios = data;
      }, error => {
        console.log(error);
      }, () => {
        this.loaderservice.isLoading = false;
      });
      this.showDateInput = true;
     }
  }
  onScroll() {
    ++this.pagina;
    console.log(this.pagina);
    if (this.parametropesquisa === 'matricula') {
      this.campopesquisa.nativeElement.placeholder = 'Digite a Matrícula do Funcionário';
    } else if (this.parametropesquisa === 'endereco')  {
      this.auth.ListarPorEndereco(this.campopesquisa.nativeElement.value, this.pagina).subscribe(
        data => {
          console.log(data);
        this.usuarios = data;
      }, error => {
        console.log(error);
      }, () => {
        this.loaderservice.isLoading = false;
      });
    } else if (this.parametropesquisa === 'email') {
      this.subscription = this.auth.ListarPorEmail(this.campopesquisa.nativeElement.value, this.pagina)
      .subscribe(data => {
        data.map( user => {
          this.usuarios.push(user);
        });
      });
    } else if (this.parametropesquisa === 'nome') {
   this.subscription = this.auth.ListarUsuariosPorNome(this.campopesquisa.nativeElement.value, this.pagina)
     .subscribe(data => {
       data.map( user => {
         this.usuarios.push(user);
       });
     });
    }
  }
  open() {
    const modalRef = this.modalService.open(FuncionarioCadastroComponent, {size: 'lg', windowClass: 'modal-adaptive'});
  }
  AbrirEditar(user: User) {
    const modalRef = this.modalService.open(FuncionarioEditarComponent, {size: 'lg', windowClass: 'modal-adaptive'});
    modalRef.componentInstance.useredit = user;
  }
}
