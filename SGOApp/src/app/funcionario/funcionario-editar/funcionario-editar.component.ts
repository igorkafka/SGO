import { AuthService } from './../../core/auth.service';
import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal, NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, NgForm, AbstractControl } from '@angular/forms';
import { RegexExpression } from '../../Shared/RegexExpression';
import { User } from '../../Models/User';
import { RegisterUser } from '../../Models/RegisterUser.model';

@Component({
  selector: 'app-funcionario-editar',
  templateUrl: './funcionario-editar.component.html',
  styleUrls: ['./funcionario-editar.component.css']
})
export class FuncionarioEditarComponent implements OnInit {
    funcionarioeditarform: FormGroup;
    @Input() useredit: User;
  constructor(public activeModal: NgbActiveModal, private auth: AuthService,  config: NgbDatepickerConfig) {
    config.minDate = {year: 1900, month: 1, day: 1};
    config.maxDate = {year: 2099, month: 12, day: 31};
   }
  ngOnInit() {
    this.initform();
  }
  passwordConfirming(c: AbstractControl): any {
    if (!c.parent || !c) { return; }
    const pwd = c.parent.get('password');
    const cpwd = c.parent.get('confirmpassword');
    if (!pwd || !cpwd) { return ; }
    if (pwd.value !== cpwd.value) {
        return { invalid: true };
    }
  }
  initform() {
    const DataNascimento = new Date(this.useredit.dataNascimento);
    console.log(DataNascimento);
    this.funcionarioeditarform =  new FormGroup({
      'id': new FormControl(this.useredit.id),
      'nome': new FormControl(this.useredit.userName, [Validators.required]),
      'datanascimento': new FormControl({day: DataNascimento.getDate(), month: DataNascimento.getMonth() + 1,
        year: DataNascimento.getFullYear()}, [Validators.required]),
      'endereco': new FormControl(this.useredit.endereco, [Validators.required]),
      'sexo': new FormControl(this.useredit.sexo ? 'masculino' : 'feminino' , [Validators.required]),
      'matricula': new FormControl(this.useredit.matricula, [Validators.required]),
      'email': new FormControl(this.useredit.email, [Validators.required, Validators.pattern(RegexExpression.EmailRegex())]),
      'confirmpassword': new FormControl('' , [Validators.required, Validators.minLength(4),
         this.passwordConfirming] ),
      'password': new FormControl('', [Validators.required])
    });
  }
  onSubmit(form) {
    if (form.valid) {
      const user = new RegisterUser();
      user.id = form.value.id;
      user.userName = form.value.nome;
      user.endereco = form.value.endereco;
      user.email = form.value.email;
      user.matricula = form.value.matricula;
      user.Password = form.value.password;
      user.dataNascimento =
      new   Date(form.value.datanascimento.year, form.value.datanascimento.month - 1,
      form.value.datanascimento.day);
      if (form.value.sexo === 'masculino') {
        user.sexo = true;
      } else {
        user.sexo = false;
      }
      this.auth.Update(user.id, user).subscribe(data => {
        console.log(data);
      }, error => {
        console.log(error);
      }, () => {
        this.activeModal.close();
      });
    }
  }
}
