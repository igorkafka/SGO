import { Obito } from './obito';

export class Causa {
  id: number;
  nome: string;
  obitos: Obito[];
}
