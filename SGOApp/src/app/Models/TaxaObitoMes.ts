import { TaxaObito } from './taxaobito';

export class TaxaObitoMes extends TaxaObito {
  Ano: number;
  Mes: number;
}
