import { Clinica } from './clinica';
import { Paciente } from './paciente';
import { Medico } from './medico.model';
import { User } from './User';
import { Causa } from './causa';

export class Obito {
  constructor(public paciente: Paciente) {

  }
  numeracao: string;
  numeroCaixa: string;
  numeroOrdem: number;
  medico: Medico;
  clinica: Clinica;
  funcionario: User;
  dataFalecimento: Date;
  descricaoEspecial: string;
  causa: Causa;
}
export enum ValoresParametrosObito {
  pacientenome = 'pacientenome',
  sexo = 'sexo',
  numeracao = 'numeracao',
  numeroregistro = 'numeroregistro',
  numerocaixa = 'numerocaixa',
  mediconome = 'mediconome',
  clinicanome = 'clinicanome',
  numeroordem = 'numeroordem',
  datanascimento = 'datanascimento',
  datafalecimento = 'datafalecimento',
  funcionarionome = 'funcionarionome',
}
