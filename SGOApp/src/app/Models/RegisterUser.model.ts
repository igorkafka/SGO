import { IPessoa } from './IPessoa';
import { User } from './User';

export class RegisterUser extends User implements IPessoa {
    nome: string;
    dataNascimento: Date;
    userName: string;
    userEmail: string;
    Password: string;
    Perfil: string;
    ConfirmPassword;
}
