export  interface IPessoa {
  id: string;
  nome: string;
  sexo: boolean;
  dataNascimento: Date;
}
