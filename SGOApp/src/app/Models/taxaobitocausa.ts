import { Causa } from './causa';
import { TaxaObito } from './taxaobito';

export class TaxaObitoCausa extends TaxaObito {
  causa: Causa;
}
