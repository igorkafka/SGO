import { Medico } from './medico.model';
export class Especialidade {
  id: number;
  nome: string;
  medicos: Medico[];
}
