import { IPessoa } from './IPessoa';

export class User implements IPessoa {
  id: string;
  nome: string;
  sexo: boolean;
  dataNascimento: Date;
  endereco: string;
  matricula: string;
  email: string;
  userName: string;
}
