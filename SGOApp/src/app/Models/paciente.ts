import { Pessoa } from './pessoa';

export class Paciente extends Pessoa {
  numeroRegistro;
  endereco: string;
}
