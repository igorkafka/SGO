import { Obito } from './obito';

export class Clinica {
     id: number;
     nome: string;
     descricao: string;
     obitos: Obito[];
}
export enum ValoresParametrosClinica {
  nome = 'nome',
  descricao = 'descricao'
}
