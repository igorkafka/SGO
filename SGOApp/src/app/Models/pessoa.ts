export abstract class Pessoa {
  id: number;
  nome: string;
  sexo: boolean;
  dataNascimento: Date;
}
