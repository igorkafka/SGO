import { Especialidade } from './especialidade';
import { Pessoa } from './pessoa';

export class Medico extends Pessoa {
  crm: string;
  especialidade: Especialidade;
}
export enum ValoresParametrosMedico {
  nome = 'nome',
  crm = 'crm',
  especialidade = 'especialidade',
  datanascimento = 'datanascimento'
}
