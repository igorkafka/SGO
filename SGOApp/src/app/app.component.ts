import { Paciente } from './Models/paciente';
import { Component } from '@angular/core';
import { AuthService } from './core/auth.service';
import { Router } from '@angular/router';
import { Token } from './Models/Token';
import { ProfileService } from './Shared/profile.service';
import { LoaderService } from './Shared/loader.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(public auth: AuthService, public loaderservice: LoaderService,
    private router: Router, private profileservice: ProfileService) {
    if (localStorage.getItem('Authenticated') === 'true') {
      this.auth.Authenticated = true;
      this.auth.token = new Token();
      this.auth.token.authenticated =  (localStorage.getItem('Authenticated') === 'true');
     this.auth.token.accessToken =  localStorage.getItem('accessToken');
    this.auth.token.created =  localStorage.getItem('created');
    this.auth.token.expiration =  localStorage.getItem('expiration');
    this.auth.token.id = localStorage.getItem('id');
    this.profileservice.profile.email = localStorage.getItem('email');
   this.auth.token.permissao =   localStorage.getItem('permissao');
      this.router.navigate(['/']);
    } else {
      this.router.navigate(['/login']);
    }
  }
}
