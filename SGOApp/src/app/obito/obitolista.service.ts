import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Obito } from '../Models/obito';

@Injectable({
  providedIn: 'root'
})
export class ObitolistaService {
  obitolistchanged: Subject<Obito[]> = new Subject();
  private obitos: Obito[] = new Array<Obito>();
  constructor() {}
  Adicionar(listaDeObitos: Obito[]) {
    this.obitos.splice(this.obitos.length - 1, 0, ...listaDeObitos);
    this.obitolistchanged.next(this.obitos);
  }
  AlterarLista(obito: Obito) {
    this.obitos.forEach((element, i) => {
      if (element.numeroOrdem === obito.numeroOrdem) {
        console.log(obito);
        this.obitos[i] = obito;
      }
    });
  }
  SetObitos(listaDeObitos: Obito[]) {
    this.obitos = listaDeObitos;
    this.obitolistchanged.next(this.obitos);
  }
}
