import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Obito } from 'src/app/Models/obito';

@Component({
  selector: 'app-obito-detalhe',
  templateUrl: './obito-detalhe.component.html',
  styleUrls: ['./obito-detalhe.component.css']
})
export class ObitoDetalheComponent implements OnInit {
  obitodetalhes: Obito;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
