import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObitoDetalheComponent } from './obito-detalhe.component';

describe('ObitoDetalheComponent', () => {
  let component: ObitoDetalheComponent;
  let fixture: ComponentFixture<ObitoDetalheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObitoDetalheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObitoDetalheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
