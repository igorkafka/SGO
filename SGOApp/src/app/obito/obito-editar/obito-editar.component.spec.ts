import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObitoEditarComponent } from './obito-editar.component';

describe('ObitoEditarComponent', () => {
  let component: ObitoEditarComponent;
  let fixture: ComponentFixture<ObitoEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObitoEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObitoEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
