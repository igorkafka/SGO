import { ObitolistaService } from './../obitolista.service';
import { Clinica } from './../../Models/clinica';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { AuthService } from 'src/app/core/auth.service';
import { LoaderService } from 'src/app/Shared/loader.service';
import { NgbDatepickerConfig, NgbActiveModal, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { ObitoService } from '../obito.service';
import { FormGroup, NgForm, FormControl, Validators } from '@angular/forms';
import { Medico } from 'src/app/Models/medico.model';
import { Paciente } from 'src/app/Models/paciente';
import { Obito } from 'src/app/Models/obito';
import { User } from 'src/app/Models/User';
import { Causa } from 'src/app/Models/causa';

@Component({
  selector: 'app-obito-editar',
  templateUrl: './obito-editar.component.html',
  styleUrls: ['./obito-editar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ObitoEditarComponent implements OnInit {
  @ViewChild('t') tab: NgbTabset;
  obitoeditar: Obito;
  obitoeditarform: FormGroup;
  constructor(private auth: AuthService, private loaderservice: LoaderService, config: NgbDatepickerConfig,
    public activeModal: NgbActiveModal, private obitoservice: ObitoService,
    private obitolistaservice: ObitolistaService) { }
    ngOnInit() {
      console.log(this.obitoeditar);
      this.initform();
    }
    onSubmit(form) {
      if (form.valid) {
        const paciente = new Paciente();
        paciente.id = this.obitoeditar.paciente.id;
         paciente.dataNascimento = new Date(form.value.datanascimento.year, form.value.datanascimento.month - 1,
          form.value.datanascimento.day);
        paciente.nome = form.value.nomepaciente;
        if (form.value.sexo === 'masculino') {
          paciente.sexo = true;
        } else {
          paciente.sexo = false;
        }
        const clinica = new Clinica();
        clinica.id = form.value.clinicaid;
        clinica.nome = form.value.clinicanome;
        paciente.numeroRegistro = form.value.numeroregistro;
        const medico = new Medico();
        medico.id = form.value.medicoid;
        medico.nome = form.value.mediconome;
        const causa = new Causa();
        causa.nome = form.value.causanome;
        causa.id = form.value.causaid;
        const obito = new Obito(paciente);
        obito.numeroOrdem = this.obitoeditar.numeroOrdem;
        obito.numeracao = form.value.numeracao;
        obito.numeroCaixa = form.value.numerocaixa;
        obito.dataFalecimento = new Date(form.value.datafalecimento.year, form.value.datafalecimento.month - 1 ,
          form.value.datafalecimento.day, form.value.horario.hour, form.value.horario.minute);
        obito.medico = medico;
        obito.clinica = clinica;
        obito.causa = causa;
        obito.funcionario = new User();
        obito.funcionario.id = this.auth.token.id;
        this.loaderservice.isLoading = true;
        this.obitoservice.Update(obito).subscribe(data => {
          this.obitolistaservice.AlterarLista(obito);
        }, error => {
          this.loaderservice.isLoading = false;
          console.log(error);
        }, () => {
          this.loaderservice.isLoading = false;
          this.activeModal.close();
        });
      } else {
        this.obitoeditarform.controls['nomepaciente'].markAsTouched();
        this.obitoeditarform.controls['numeracao'].markAsTouched();
        this.obitoeditarform.controls['numerocaixa'].markAsTouched();
        this.obitoeditarform.controls['numeroregistro'].markAsTouched();
        this.obitoeditarform.controls['datanascimento'].markAsTouched();
        this.obitoeditarform.controls['datafalecimento'].markAsTouched();
        this.obitoeditarform.controls['mediconome'].markAsTouched();
        this.obitoeditarform.controls['clinicanome'].markAsTouched();
        this.obitoeditarform.controls['horario'].markAsTouched();
        this.obitoeditarform.controls['causanome'].markAsTouched();
        this.obitoeditarform.controls['causaid'].markAsTouched();
      }
    }
    initform() {
      const dataFalecimento = new Date(this.obitoeditar.dataFalecimento);
      const dataNascimento = new Date(this.obitoeditar.paciente.dataNascimento);
      console.log(dataFalecimento.getHours());
      this.obitoeditarform = new FormGroup({
        'causanome': new FormControl(this.obitoeditar.causa.nome, [Validators.required]),
        'causaid': new FormControl(this.obitoeditar.causa.id, [Validators.required]),
        'nomepaciente': new FormControl(this.obitoeditar.paciente.nome, Validators.required),
        'numeroregistro': new FormControl(this.obitoeditar.paciente.numeroRegistro, [Validators.required]),
        'numeracao': new FormControl(this.obitoeditar.numeracao, [Validators.required]),
        'numerocaixa': new FormControl(this.obitoeditar.numeroCaixa, [Validators.required]),
        'sexo': new FormControl(this.obitoeditar.paciente.sexo ? 'masculino' : 'feminino'),
        'horario': new FormControl({hour: dataFalecimento.getHours(),
          minute: dataFalecimento.getMinutes()}, [Validators.required]),
        'datanascimento': new FormControl({day: dataNascimento.getDate(), month: dataNascimento.getMonth() + 1,
          year: dataNascimento.getFullYear()}, [Validators.required]),
        'datafalecimento': new FormControl({day: dataFalecimento.getDate(), month: dataFalecimento.getMonth() + 1,
        year: dataFalecimento.getFullYear()}, [Validators.required]),
        'medicoid': new FormControl(this.obitoeditar.medico.id, [Validators.required]),
        'mediconome': new FormControl(this.obitoeditar.medico.nome, [Validators.required]),
        'clinicaid': new FormControl(this.obitoeditar.clinica.id, [Validators.required]),
        'clinicanome': new FormControl(this.obitoeditar.clinica.nome, [Validators.required])
      });
    }
  SetMedico(medicoselecionado: Medico) {
    const medico = medicoselecionado;
    this.tab.select('tab-cadastro');
    this.obitoeditarform.controls['medicoid'].setValue(medico.id);
    this.obitoeditarform.controls['mediconome'].setValue(medico.nome);
  }
  SetClinica(clinicaselecionada: Clinica) {
    const clinica = clinicaselecionada;
    this.tab.select('tab-cadastro');
    this.obitoeditarform.controls['clinicaid'].setValue(clinica.id);
    this.obitoeditarform.controls['clinicanome'].setValue(clinica.nome);
  }
  SetCausa(causaselecionada: Causa) {
    const causa = causaselecionada;
    this.tab.select('tab-cadastro');
    this.obitoeditarform.controls['causaid'].setValue(causa.id);
    this.obitoeditarform.controls['causanome'].setValue(causa.nome);
  }
}
