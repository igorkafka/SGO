import { ObitoEditarComponent } from './obito-editar/obito-editar.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ObitoComponent } from './obito.component';
import { ObitoListaComponent } from './obito-lista/obito-lista.component';
import { ObitoCadastrarComponent } from './obito-cadastrar/obito-cadastrar.component';
import { ObitoDetalheComponent } from './obito-detalhe/obito-detalhe.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'obito', component: ObitoComponent, children: [
        { path:  '', component: ObitoListaComponent},
        { path: 'editar', component: ObitoEditarComponent},
        { path: 'cadastro', component: ObitoCadastrarComponent},
        { path: 'detalhes', component: ObitoDetalheComponent}
      ]}
    ])
  ],
  exports: [RouterModule]
})
export class ObitoRoutingModule {

}
