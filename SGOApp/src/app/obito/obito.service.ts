import { LoaderService } from './../Shared/loader.service';
import { Medico } from './../Models/medico.model';
import { ClinicaListaService } from './../clinica/clinica-lista.service';
import { MedicoListaService } from './../medico/medicolista.service';
import { Observable } from 'rxjs';
import { AuthService } from './../core/auth.service';
import { Obito, ValoresParametrosObito } from './../Models/obito';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ObitolistaService } from './obitolista.service';

@Injectable({
  providedIn: 'root'
})
export class ObitoService {

  constructor(private http: HttpClient, private auth: AuthService, private medicolistaservice:
    MedicoListaService,  private loader: LoaderService, private obitolistaservice: ObitolistaService,
    private clinicalistaservice: ClinicaListaService ) {
  }
  Pesquisar(valor: any, parametro: string, pagina: number) {
    switch (parametro) {
      case ValoresParametrosObito.pacientenome :
        this.ListarPorNomePaciente(valor, pagina);
        break;
      case ValoresParametrosObito.sexo :
      break;
      case ValoresParametrosObito.numeroregistro:
      this.ListarPorNumeroDeRegistro(valor, pagina);
      break;
      case ValoresParametrosObito.numeracao:
      this.ListarPorNumeracao(valor, pagina);
      break;
      case ValoresParametrosObito.numerocaixa:
      this.ListarPorCaixa(valor, pagina);
      break;
      case ValoresParametrosObito.numeroordem:
      this.ListarPorNumeroOrdem(valor, pagina);
      break;
      case ValoresParametrosObito.datafalecimento:
      this.ListarPorDataFalecimento(valor, pagina);
      break;
      case ValoresParametrosObito.datanascimento:
      this.ListarPorDataDeNascimento(valor, pagina);
      break;
      case ValoresParametrosObito.mediconome:
      this.ListarPorNomeMedico(valor, pagina);
      break;
      case ValoresParametrosObito.clinicanome:
      this.ListarPorNomeDaClinica(valor, pagina);
      break;
      default:
        break;
    }
  }
  Create(obito: Obito): Observable<void> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    return this.http.post<void>('/api/obito/create', obito, {headers: headers});
  }
  Update(obito: Obito) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    return this.http.put<void>('/api/obito/update', obito, {headers: headers});
  }
  ListarPorNomePaciente(nomepaciente: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Obito[]>('/api/obito/listarporenomepaciente?nomepaciente=' + nomepaciente
    + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      if (pagina === 1) {
        this.obitolistaservice.SetObitos(data);
      } else {
        this.obitolistaservice.Adicionar(data);
      }
    });
  }
  ListarPorNomeMedico(nomemedico: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Obito[]>('/api/obito/listarpornomemedico?nomemedico=' + nomemedico
    + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      if (pagina === 1) {
        this.obitolistaservice.SetObitos(data);
      } else {
        this.obitolistaservice.Adicionar(data);
      }
    });
  }
  ListarPorCaixa(caixa: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Obito[]>('/api/obito/listarpelonumerocaixa?caixa=' + caixa
    + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      if (pagina === 1) {
        this.obitolistaservice.SetObitos(data);
      } else {
        this.obitolistaservice.Adicionar(data);
      }
    });
  }
  ListarPorNumeroOrdem(numeroordem: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Obito[]>('/api/obito/listarpornumeroordem?numeroordem=' + numeroordem
    + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      if (pagina === 1) {
        this.obitolistaservice.SetObitos(data);
      } else {
        this.obitolistaservice.Adicionar(data);
      }
    });
  }
  ListarPorNomeDaClinica(nomeclinica: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Obito[]>('/api/obito/listarpornomeclinica?nomeclinica='
     + nomeclinica + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      if (pagina === 1) {
        this.obitolistaservice.SetObitos(data);
      } else {
        this.obitolistaservice.Adicionar(data);
      }
    });
  }
  ListarPorNumeroDeRegistro(numeroregistro: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Obito[]>('/api/obito/listarpornumeroregistro?numeroregistro='
     + numeroregistro + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      if (pagina === 1) {
        this.obitolistaservice.SetObitos(data);
      } else {
        this.obitolistaservice.Adicionar(data);
      }
    });
  }
  ListarPorDataFalecimento(datafalecimento: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Obito[]>('/api/obito/listarpordatafalecimento?datafalecimento='
    + datafalecimento + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      if (pagina === 1) {
        this.obitolistaservice.SetObitos(data);
      } else {
        this.obitolistaservice.Adicionar(data);
      }
    });
  }
  ListarPorDataDeNascimento(datanascimento: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Obito[]>('/api/obito/listarpordatanascimento?datanascimento='
    + datanascimento + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      if (pagina === 1) {
        this.obitolistaservice.SetObitos(data);
      } else {
        this.obitolistaservice.Adicionar(data);
      }
    });
  }
  ListarMedicos(nome: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Medico[]>('/api/obito/listarmedicos?nomemedico=' + nome +
    '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      if (pagina === 1) {
        this.medicolistaservice.SetMedicos(data);
      } else {
        this.medicolistaservice.Adicionar(data);
      }
    });
  }
  ListarPorNumeracao(numeracao: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Obito[]>('/api/obito/listarpelanumeracao?numeracao=' + numeracao
    + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      if (pagina === 1) {
        this.obitolistaservice.SetObitos(data);
      } else {
        this.obitolistaservice.Adicionar(data);
      }
    });
  }
}
