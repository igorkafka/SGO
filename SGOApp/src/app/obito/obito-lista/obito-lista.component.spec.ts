import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObitoListaComponent } from './obito-lista.component';

describe('ObitoListaComponent', () => {
  let component: ObitoListaComponent;
  let fixture: ComponentFixture<ObitoListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObitoListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObitoListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
