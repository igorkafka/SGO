import { ObitoDetalheComponent } from './../obito-detalhe/obito-detalhe.component';
import { ObitolistaService } from './../obitolista.service';
import { ValoresParametrosObito } from './../../Models/obito';
import { ObitoCadastrarComponent } from './../obito-cadastrar/obito-cadastrar.component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Obito } from '../../Models/obito';
import { Subscription } from 'rxjs';
import { AuthService } from '../../core/auth.service';
import { NgbDatepickerConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ObitoService } from './../obito.service';
import { ObitoEditarComponent } from '../obito-editar/obito-editar.component';

@Component({
  selector: 'app-obito-lista',
  templateUrl: './obito-lista.component.html',
  styleUrls: ['./obito-lista.component.css']
})
export class ObitoListaComponent implements OnInit {
  obitos: Obito[];
  parametropesquisa: string = 'pacientenome';
  subscription: Subscription;
  pagina: number = 1;
  mostrarcampo: string = 'outro';
  @ViewChild('camposelect') camposelect: ElementRef;
  @ViewChild('campoinput') campopesquisa: ElementRef;
  @ViewChild('campoinputdata') campoinputdata: ElementRef;
  constructor(private modalService: NgbModal,
    config: NgbDatepickerConfig, private auth: AuthService, private obitoservice: ObitoService,
    private obitolistaservice: ObitolistaService) { }
  ngOnInit() {
    this.obitolistaservice.obitolistchanged.subscribe(data => {
      this.obitos = data;
    });
  }
  MudarParametroPesquisa(event) {
    this.parametropesquisa = event.target.value;
    this.pagina = 1;
    this.mostrarcampo = 'outro';
    switch (this.parametropesquisa) {
      case ValoresParametrosObito.pacientenome :
      this.campopesquisa.nativeElement.placeholder = 'Digite o Nome do Paciente';
        break;
      case ValoresParametrosObito.sexo :
      this.mostrarcampo = 'select';
      break;
      case ValoresParametrosObito.numeroregistro:
      this.campopesquisa.nativeElement.placeholder = 'Digite o número do registro';
      break;
      case ValoresParametrosObito.numeracao:
      this.campopesquisa.nativeElement.placeholder = 'Digite a Numeração';
      break;
      case ValoresParametrosObito.numerocaixa:
      this.campopesquisa.nativeElement.placeholder = 'Digite o Número da Caixa';
      break;
      case ValoresParametrosObito.numeroordem:
      this.campopesquisa.nativeElement.placeholder = 'Digite o Número da Ordem';
      break;
      case ValoresParametrosObito.datafalecimento:
      this.mostrarcampo = 'data';
      break;
      case ValoresParametrosObito.datanascimento:
      this.mostrarcampo = 'data';
      break;
      case ValoresParametrosObito.mediconome:
      this.campopesquisa.nativeElement.placeholder = 'Digite o Nome do Médico';
      break;
      case ValoresParametrosObito.clinicanome:
      this.campopesquisa.nativeElement.placeholder = 'Digite o Nome da Clínica';
      break;
      default:
        break;
    }
  }
  Pesquisar() {
    this.pagina = 1;
    this.obitoservice.Pesquisar(this.ValorPesquisa(), this.parametropesquisa, this.pagina);
  }
  ValorPesquisa() {
    switch (this.mostrarcampo) {
      case 'outro':
      return this.campopesquisa.nativeElement.value;
      case 'data':
      return this.campoinputdata.nativeElement.value;
      case 'select':
      return this.camposelect.nativeElement.value;
      default:
        return null;
    }
  }
  onScroll() {

  }
  AbrirEditar(obito: Obito) {
    const modalRef = this.modalService.open(ObitoEditarComponent, {size: 'lg', windowClass: 'modal-adaptive'});
    modalRef.componentInstance.obitoeditar = obito;
  }
  AbrirRecurso(obito: Obito) {
    const modalRef = this.modalService.open(ObitoDetalheComponent, {size: 'lg', windowClass: 'modal-adaptive'});
    modalRef.componentInstance.obitodetalhes = obito;
  }
  open() {
    const modalRef = this.modalService.open(ObitoCadastrarComponent, {size: 'lg', windowClass: 'modal-adaptive'});
  }
}
