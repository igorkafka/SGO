import { TestBed, inject } from '@angular/core/testing';

import { ObitolistaService } from './obitolista.service';

describe('ObitolistaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ObitolistaService]
    });
  });

  it('should be created', inject([ObitolistaService], (service: ObitolistaService) => {
    expect(service).toBeTruthy();
  }));
});
