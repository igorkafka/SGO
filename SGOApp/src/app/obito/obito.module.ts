import { CausaListaService } from './../causa/causa-lista.service';
import { CausaService } from './../causa/causa.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ObitoRoutingModule } from './obito-routing.module';
import { ObitoComponent } from './obito.component';
import { ObitoListaComponent } from './obito-lista/obito-lista.component';
import { ObitoCadastrarComponent } from './obito-cadastrar/obito-cadastrar.component';
import { ObitoEditarComponent } from './obito-editar/obito-editar.component';
import { NgbModalModule, NgbDatepickerModule, NgbTabsetModule, NgbTimepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ObitoPesquisaMedicoComponent } from './obito-cadastrar/obito-pesquisa-medico/obito-pesquisa-medico.component';
import { ObitoPesquisaClinicaComponent } from './obito-cadastrar/obito-pesquisa-clinica/obito-pesquisa-clinica.component';
import { ObitoService } from './obito.service';
import { ObitolistaService } from './obitolista.service';
import { MedicoListaService } from '../medico/medicolista.service';
import { ClinicaListaService } from './../clinica/clinica-lista.service';
import { ObitoPesquisaCausaComponent } from './obito-cadastrar/obito-pesquisa-causa/obito-pesquisa-causa.component';
import { ObitoDetalheComponent } from './obito-detalhe/obito-detalhe.component';
@NgModule({
  imports: [
    CommonModule, FormsModule,
    ObitoRoutingModule, NgbDatepickerModule, NgbTimepickerModule,
    ReactiveFormsModule, InfiniteScrollModule, NgbModalModule, NgbTabsetModule
  ],
  declarations: [ObitoComponent, ObitoListaComponent, ObitoCadastrarComponent, ObitoEditarComponent,
    ObitoPesquisaMedicoComponent, ObitoPesquisaClinicaComponent, ObitoPesquisaCausaComponent, ObitoDetalheComponent],
    providers: [ObitoService, ObitolistaService, MedicoListaService, CausaService, CausaListaService],
    exports: [ObitoPesquisaCausaComponent]
})
export class ObitoModule { }
