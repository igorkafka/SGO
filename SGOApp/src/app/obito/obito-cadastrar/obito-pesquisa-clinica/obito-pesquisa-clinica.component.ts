import { ClinicaListaService } from './../../../clinica/clinica-lista.service';
import { Component, OnInit, Output, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { Clinica } from '../../../Models/clinica';
import { ClinicaService } from '../../../clinica/clinica.service';

@Component({
  selector: 'app-obito-pesquisa-clinica',
  templateUrl: './obito-pesquisa-clinica.component.html',
  styleUrls: ['./obito-pesquisa-clinica.component.css'],
  providers: [ClinicaService, ClinicaListaService]
})
export class ObitoPesquisaClinicaComponent implements OnInit {
  clinicas: Clinica[];
  @Output('selecionarClinica') selecionarClinica = new EventEmitter();
  @ViewChild('campoinput') campoinput: ElementRef;
  pagina: number = 1;
  constructor(private clinicaservice: ClinicaService, private clinicalista: ClinicaListaService) { }

  ngOnInit() {
    this.clinicalista.clinicalistachanged.subscribe(data => {
      this.clinicas = data;
    });
  }
  Pesquisar() {
    this.pagina = 1;
    this.clinicaservice.ListarPorNome(this.campoinput.nativeElement.value, this.pagina);
  }
  onScroll() {
    this.pagina++;
    this.clinicaservice.ListarPorNome(this.campoinput.nativeElement.value, this.pagina);
  }
  SelecionarClinica(clinica: Clinica) {
    this.selecionarClinica.emit(clinica);
  }
}
