import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObitoPesquisaClinicaComponent } from './obito-pesquisa-clinica.component';

describe('ObitoPesquisaClinicaComponent', () => {
  let component: ObitoPesquisaClinicaComponent;
  let fixture: ComponentFixture<ObitoPesquisaClinicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObitoPesquisaClinicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObitoPesquisaClinicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
