import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObitoCadastrarComponent } from './obito-cadastrar.component';

describe('ObitoCadastrarComponent', () => {
  let component: ObitoCadastrarComponent;
  let fixture: ComponentFixture<ObitoCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObitoCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObitoCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
