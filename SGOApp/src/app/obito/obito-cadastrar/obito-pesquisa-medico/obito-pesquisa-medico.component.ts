import { Medico } from './../../../Models/medico.model';
import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { MedicoListaService } from './../../../medico/medicolista.service';
import { ObitoService } from '../../obito.service';
@Component({
  selector: 'app-obito-pesquisa-medico',
  templateUrl: './obito-pesquisa-medico.component.html',
  styleUrls: ['./obito-pesquisa-medico.component.css'],
  providers: []
})
export class ObitoPesquisaMedicoComponent implements OnInit {
  medicos: Medico[];
  @Output('selecionarMedico') selecionarMedico = new EventEmitter();
  @ViewChild('campoinput') campoinput: ElementRef;
  pagina: number = 1;
  constructor(private medicolistaservice: MedicoListaService, private obitoservice: ObitoService) { }
  ngOnInit() {
    this.medicolistaservice.medicolistchanged.subscribe(data => {
      this.medicos = data;
    }, error => {
    }, () => {
    });
  }
  Pesquisar() {
    this.pagina = 1;
    this.obitoservice.ListarMedicos(this.campoinput.nativeElement.value, this.pagina);
  }
  onScroll() {
    this.pagina++;
    this.obitoservice.ListarMedicos(this.campoinput.nativeElement.value, this.pagina);
  }
  SelecionarMedico(medico: Medico) {
    this.selecionarMedico.emit(medico);
  }
}
