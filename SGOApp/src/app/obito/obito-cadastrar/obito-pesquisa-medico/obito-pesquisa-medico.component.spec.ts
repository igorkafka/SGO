import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObitoPesquisaMedicoComponent } from './obito-pesquisa-medico.component';

describe('ObitoPesquisaMedicoComponent', () => {
  let component: ObitoPesquisaMedicoComponent;
  let fixture: ComponentFixture<ObitoPesquisaMedicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObitoPesquisaMedicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObitoPesquisaMedicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
