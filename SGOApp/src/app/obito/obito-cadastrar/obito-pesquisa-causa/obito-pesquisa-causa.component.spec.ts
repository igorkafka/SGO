import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ObitoPesquisaCausaComponent } from './obito-pesquisa-causa.component';

describe('ObitoPesquisaCausaComponent', () => {
  let component: ObitoPesquisaCausaComponent;
  let fixture: ComponentFixture<ObitoPesquisaCausaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ObitoPesquisaCausaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ObitoPesquisaCausaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
