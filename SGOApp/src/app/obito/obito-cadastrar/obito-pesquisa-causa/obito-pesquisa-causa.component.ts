import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Causa } from 'src/app/Models/causa';
import { CausaService } from 'src/app/causa/causa.service';
import { CausaListaService } from 'src/app/causa/causa-lista.service';

@Component({
  selector: 'app-obito-pesquisa-causa',
  templateUrl: './obito-pesquisa-causa.component.html',
  styleUrls: ['./obito-pesquisa-causa.component.css']
})
export class ObitoPesquisaCausaComponent implements OnInit {
  causas: Causa[];
  @Output('selecionarcausa') selecionarcausa = new EventEmitter();
  @ViewChild('campoinput') campoinput: ElementRef;
  pagina: number = 1;
  isCreateOrEditObito;
  constructor(private causaservice: CausaService, private causalista: CausaListaService) { }

  ngOnInit() {
    this.causalista.causalistchanged.subscribe(data => {
      this.causas = data;
    });
  }
  Pesquisar() {
    this.pagina = 1;
    this.causaservice.ListarCausas(this.campoinput.nativeElement.value, this.pagina);
  }
  onScroll() {
    this.pagina++;
    this.causaservice.ListarCausas(this.campoinput.nativeElement.value, this.pagina);
  }
  SelecionarCausa(causa: Causa) {
    this.selecionarcausa.emit(causa);
  }
}
