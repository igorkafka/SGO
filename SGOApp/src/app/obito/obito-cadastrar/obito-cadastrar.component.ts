import { Causa } from './../../Models/causa';
import { Clinica } from './../../Models/clinica';
import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation } from '@angular/core';
import { ObitoService } from './../obito.service';
import { AuthService } from './../../core/auth.service';
import { LoaderService } from '../../Shared/loader.service';
import { NgbActiveModal, NgbTabChangeEvent, NgbDatepickerConfig, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { Paciente } from './../../Models/paciente';
import { Medico } from '../../Models/medico.model';
import { Obito } from '../../Models/obito';
import { User } from '../../Models/User';

@Component({
  selector: 'app-obito-cadastrar',
  templateUrl: './obito-cadastrar.component.html',
  styleUrls: ['./obito-cadastrar.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ObitoCadastrarComponent implements OnInit {
  obitocadastroform: FormGroup;
  @ViewChild('btncadastrar') btncadastrar: ElementRef;
  @ViewChild('t') tab: NgbTabset;
  constructor(private auth: AuthService, private loaderservice: LoaderService, config: NgbDatepickerConfig,
     public activeModal: NgbActiveModal, private obitoservice: ObitoService) {
      config.minDate = {year: 1900, month: 1, day: 1};
      config.maxDate = {year: 2099, month: 12, day: 31};
     }

  ngOnInit() {
    this.initform();
  }
  onSubmit(form) {
    if (form.valid) {
      const paciente = new Paciente();
       paciente.dataNascimento = new Date(form.value.datanascimento.year, form.value.datanascimento.month - 1,
        form.value.datanascimento.day);
      paciente.nome = form.value.nomepaciente;
      paciente.endereco = form.value.endereco;
      if (form.value.sexo === 'masculino') {
        paciente.sexo = true;
      } else {
        paciente.sexo = false;
      }
      const clinica = new Clinica();
      clinica.id = form.value.clinicaid;
      paciente.numeroRegistro = form.value.numeroregistro;
      const medico = new Medico();
      medico.id = form.value.medicoid;
      const causa = new Causa();
      causa.nome = form.value.causanome;
      causa.id = form.value.causaid;
      const obito = new Obito(paciente);
      obito.numeracao = form.value.numeracao;
      obito.numeroCaixa = form.value.numerocaixa;
      obito.dataFalecimento = new Date(form.value.datafalecimento.year, form.value.datafalecimento.month - 1 ,
        form.value.datafalecimento.day, form.value.horario.hour, form.value.horario.minute);
      obito.medico = medico;
      obito.clinica = clinica;
      obito.causa = causa;
      obito.funcionario = new User();
      obito.funcionario.id = this.auth.token.id;
      this.loaderservice.isLoading = true;
      this.obitoservice.Create(obito).subscribe(data => {

      }, error => {
        this.loaderservice.isLoading = false;
        console.log(error);
      }, () => {
        this.loaderservice.isLoading = false;
        this.activeModal.close();
      });
    } else {
      this.obitocadastroform.controls['nomepaciente'].markAsTouched();
      this.obitocadastroform.controls['endereco'].markAsTouched();
      this.obitocadastroform.controls['causanome'].markAsTouched();
      this.obitocadastroform.controls['causaid'].markAsTouched();
      this.obitocadastroform.controls['numeracao'].markAsTouched();
      this.obitocadastroform.controls['numerocaixa'].markAsTouched();
      this.obitocadastroform.controls['numeroregistro'].markAsTouched();
      this.obitocadastroform.controls['datanascimento'].markAsTouched();
      this.obitocadastroform.controls['datafalecimento'].markAsTouched();
      this.obitocadastroform.controls['mediconome'].markAsTouched();
      this.obitocadastroform.controls['clinicanome'].markAsTouched();
      this.obitocadastroform.controls['horario'].markAsTouched();
    }
  }
  initform() {
    this.obitocadastroform = new FormGroup({
      'nomepaciente': new FormControl('', Validators.required),
      'causanome': new FormControl('', Validators.required),
      'causaid': new FormControl('', Validators.required),
      'endereco': new FormControl('', [Validators.required]),
      'numeroregistro': new FormControl('', [Validators.required]),
      'numeracao': new FormControl('', [Validators.required]),
      'numerocaixa': new FormControl('', [Validators.required]),
      'sexo': new FormControl('masculino'),
      'horario': new FormControl('', [Validators.required]),
      'datanascimento': new FormControl('', [Validators.required]),
      'datafalecimento': new FormControl('', [Validators.required]),
      'medicoid': new FormControl('', [Validators.required]),
      'mediconome': new FormControl('', [Validators.required]),
      'clinicaid': new FormControl('', [Validators.required]),
      'clinicanome': new FormControl('', [Validators.required])
    });
  }
  SetMedico(medicoselecionado: Medico) {
    const medico = medicoselecionado;
    this.tab.select('tab-cadastro');
    this.obitocadastroform.controls['medicoid'].setValue(medico.id);
    this.obitocadastroform.controls['mediconome'].setValue(medico.nome);
  }
  SetClinica(clinicaselecionada: Clinica) {
    const clinica = clinicaselecionada;
    this.tab.select('tab-cadastro');
    this.obitocadastroform.controls['clinicaid'].setValue(clinica.id);
    this.obitocadastroform.controls['clinicanome'].setValue(clinica.nome);
  }
  SetCausa(causaselecionada: Causa) {
    const causa = causaselecionada;
    this.tab.select('tab-cadastro');
    this.obitocadastroform.controls['causaid'].setValue(causa.id);
    this.obitocadastroform.controls['causanome'].setValue(causa.nome);
  }
}
