import { CausaCadastrarComponent } from './causa-cadastrar/causa-cadastrar.component';
import { CausaListaComponent } from './causa-lista/causa-lista.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CausaComponent } from './causa/causa.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'causa', component: CausaComponent, children: [
        { path: '', component: CausaListaComponent},
        { path: 'cadastrar', component: CausaCadastrarComponent}
      ]}
    ])
  ],
  exports: [RouterModule]
})
export class CausaRoutingModule {

}
