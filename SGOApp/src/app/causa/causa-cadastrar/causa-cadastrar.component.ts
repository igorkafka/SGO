import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { CausaService } from '../causa.service';
import { LoaderService } from 'src/app/Shared/loader.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Causa } from 'src/app/Models/causa';

@Component({
  selector: 'app-causa-cadastrar',
  templateUrl: './causa-cadastrar.component.html',
  styleUrls: ['./causa-cadastrar.component.css']
})
export class CausaCadastrarComponent implements OnInit {
  causacadastroform: FormGroup;
  constructor(private causaservice: CausaService,   private loaderservice: LoaderService,
    public activeModal: NgbActiveModal) {
      this.initform();
    }

  ngOnInit() {
  }
  initform() {
    this.causacadastroform = new FormGroup({
    'nome': new FormControl('', [Validators.required])
    }
    );
  }
  onSubmit(form) {
    if (form.valid) {
      const causa = new Causa();
      causa.nome = form.value.nome;
      this.loaderservice.isLoading = true;
      this.causaservice.Create(causa).subscribe(data => {

      }, error => {
        console.log(error);
        this.loaderservice.isLoading = false;
      }, () => {
        this.loaderservice.isLoading = false;
        this.activeModal.close();
      });
    } else {
      this.causacadastroform.controls['nome'].markAsTouched();
    }

  }
}
