import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CausaCadastrarComponent } from './causa-cadastrar.component';

describe('CausaCadastrarComponent', () => {
  let component: CausaCadastrarComponent;
  let fixture: ComponentFixture<CausaCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CausaCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CausaCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
