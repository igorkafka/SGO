import { TestBed, inject } from '@angular/core/testing';

import { CausaListaService } from './causa-lista.service';

describe('CausaListaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CausaListaService]
    });
  });

  it('should be created', inject([CausaListaService], (service: CausaListaService) => {
    expect(service).toBeTruthy();
  }));
});
