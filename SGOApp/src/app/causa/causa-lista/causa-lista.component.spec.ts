import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CausaListaComponent } from './causa-lista.component';

describe('CausaListaComponent', () => {
  let component: CausaListaComponent;
  let fixture: ComponentFixture<CausaListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CausaListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CausaListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
