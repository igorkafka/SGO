import { CausaCadastrarComponent } from './../causa-cadastrar/causa-cadastrar.component';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Causa } from 'src/app/Models/causa';
import { CausaListaService } from '../causa-lista.service';
import { CausaService } from '../causa.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-causa-lista',
  templateUrl: './causa-lista.component.html',
  styleUrls: ['./causa-lista.component.css']
})
export class CausaListaComponent implements OnInit {
  causas: Causa[];
  pagina: number = 1;
  @ViewChild('campoinput') campopesquisa: ElementRef;
  constructor(private listacausaservice: CausaListaService, private  causaservice: CausaService,
    private modalService: NgbModal) { }
  ngOnInit() {
    this.listacausaservice.causalistchanged.subscribe(data => {
      this.causas = data;
    });
  }
  Pesquisar() {
    this.pagina = 1;
    this.causaservice.ListarCausas(this.campopesquisa.nativeElement.value, this.pagina);
  }
  open() {
    const modalRef = this.modalService.open(CausaCadastrarComponent, {size: 'lg', windowClass: 'modal-adaptive'});
  }
  onScroll() {
    this.pagina++;
  }
}
