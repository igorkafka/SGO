import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Causa } from '../Models/causa';

@Injectable({
  providedIn: 'root'
})
export class CausaListaService {

  causalistchanged: Subject<Causa[]> = new Subject();
  private Causas: Causa[] = new Array<Causa>();
  constructor() {}
  Adicionar(listaDeCausas: Causa[]) {
    this.Causas.splice(this.Causas.length - 1, 0, ...listaDeCausas);
    this.causalistchanged.next(this.Causas);
  }
  AlterarLista(causa: Causa) {
    this.Causas.forEach((element, i) => {
      if (element.id === causa.id) {
        this.Causas[i] = causa;
      }
    });
  }
  SetCausas(listaDeCausas: Causa[]) {
    this.Causas = listaDeCausas;
    this.causalistchanged.next(this.Causas);
  }
}
