import { CausaListaService } from './causa-lista.service';
import { CausaService } from './causa.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CausaComponent } from './causa/causa.component';
import { CausaCadastrarComponent } from './causa-cadastrar/causa-cadastrar.component';
import { CausaListaComponent } from './causa-lista/causa-lista.component';
import { CausaRoutingModule } from './causa-routing.module';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    CausaRoutingModule,
    InfiniteScrollModule,
    ReactiveFormsModule,
    NgbModalModule
  ],
  declarations: [CausaComponent, CausaCadastrarComponent, CausaListaComponent],
  providers: [CausaService, CausaListaService]
})
export class CausaModule { }
