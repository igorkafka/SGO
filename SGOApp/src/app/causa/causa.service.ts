import { Observable } from 'rxjs/observable';
import { CausaListaService } from './causa-lista.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../core/auth.service';
import { LoaderService } from '../Shared/loader.service';
import { Causa } from '../Models/causa';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class CausaService {

  constructor(private http: HttpClient, private auth: AuthService, private loader: LoaderService,
    private causalistaservice: CausaListaService,   private modalService: NgbModal) { }
  ListarCausas(nome: string, pagina: number) {
    this.loader.isLoading = true;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Causa[]>('/api/causa/listarpornome?nome='
     + nome + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      this.PreencherLista(data, pagina);
    }, error => {
      console.log(error);
      this.loader.isLoading = false;
    }, () => this.loader.isLoading = false);
  }
  PreencherLista(Causas: Causa[], pagina: number) {
    if (pagina === 1) {
      this.causalistaservice.SetCausas(Causas);
    } else {
      this.causalistaservice.Adicionar(Causas);
    }
  }
  Create(causa: Causa): Observable<void> {
    console.log(causa);
    this.loader.isLoading = true;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
   return this.http.post<void>('/api/causa/create', causa, {headers: headers});
  }
}
