import { TestBed, inject } from '@angular/core/testing';

import { EnfermariaService } from './enfermaria.service';

describe('EnfermariaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EnfermariaService]
    });
  });

  it('should be created', inject([EnfermariaService], (service: EnfermariaService) => {
    expect(service).toBeTruthy();
  }));
});
