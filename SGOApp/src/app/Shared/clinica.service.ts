import { Clinica } from './../Models/clinica';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable} from 'rxjs/observable';
import { map  } from 'rxjs/operators';
import { catchError } from 'rxjs/operators/catchError';
import { Subject } from 'rxjs';
@Injectable()
export class ClinicaService {
    Clinicas = new Subject<Clinica>();
    constructor(private http: Http) {}
    ListarClinicaPorNome(nome: string): Observable<Clinica[]> {
        return this.http.get('/assets/data.json').pipe(map((res: Response) =>  res.json()),

        catchError(this.handleError));
    }
    CadastrarClinica() {
    }
    private handleError (error: any) {
        const errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
