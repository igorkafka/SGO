import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable} from 'rxjs/observable';
import { map  } from 'rxjs/operators';
import { catchError } from 'rxjs/operators/catchError';
import { Subject } from 'rxjs';

/**
* This class provides the NameList service with methods to read names and add names.
*/
@Injectable()
export class NameListService {
/**
* Creates a new NameListService with the injected Http.
* @param {Http} http - The injected Http.
* @constructor
*/
    constructor(private http: Http) {}
/**
* Returns an Observable for the HTTP GET request for the JSON resource.
* @return {string[]} The Observable for the HTTP request.
*/
    get(): Observable<string[]> {
        return this.http.get('/assets/data.json').pipe(map((res: Response) =>  res.json()),

        catchError(this.handleError));
    }
    Cadastrar() {

    }

/**
* Handle HTTP error
*/
    private handleError (error: any) {
        const errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}
