export  class  RegexExpression {
    static EmailRegex() {
        return '[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}';
    }
}
