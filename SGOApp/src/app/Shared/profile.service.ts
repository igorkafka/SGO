import { Injectable } from '@angular/core';
import { Profile } from '../Models/profile';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  profile: Profile = new Profile();
  constructor() { }
}
