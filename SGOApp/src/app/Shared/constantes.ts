

export class Anos {
 static  GetAnos () {
  const anos = new Array<string>();
  let ano = 1900;
  for (let index = 0; ano <= 2099; index++) {
     anos[index] = ano.toString();
     ano++;
  }
  return anos;
}
}
