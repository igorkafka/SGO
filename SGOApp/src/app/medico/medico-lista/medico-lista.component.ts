import { LoaderService } from './../../Shared/loader.service';
import { MedicoListaService } from './../medicolista.service';
import { MedicoService } from './../medico.service';
import { MedicoCadastroComponent } from './../medico-cadastro/medico-cadastro.component';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Medico } from '../../Models/medico.model';
import { Subscription } from 'rxjs';
import { NgbDatepickerConfig, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AuthService } from '../../core/auth.service';
import { MedicoEditarComponent } from '../medico-editar/medico-editar.component';

@Component({
  selector: 'app-medico-lista',
  templateUrl: './medico-lista.component.html',
  styleUrls: ['./medico-lista.component.css']
})
export class MedicoListaComponent implements OnInit {
  medicos: Medico[];
  parametropesquisa: string = 'nome';
  subscription: Subscription;
  pagina: number = 1;
  showDateInput: boolean = false;
  @ViewChild('campoinput') campopesquisa: ElementRef;
  @ViewChild('campoinputdatanascimento') campoinputdatanascimento: ElementRef;
  constructor(private medicolistaservice: MedicoListaService, private loader: LoaderService,
     private modalService: NgbModal, config: NgbDatepickerConfig, private medicoservice: MedicoService,
      private auth: AuthService) { }
  ngOnInit() {
    this.medicolistaservice.medicolistchanged.subscribe( data => {
      this.medicos = data;
    });
  }
  MudarParametroPesquisa(event) {
    this.parametropesquisa = event.target.value;
    this.pagina = 1;
    if (this.parametropesquisa !== 'Data') {
    this.showDateInput = false;
    }
    switch (this.parametropesquisa) {
      case 'crm':
      this.campopesquisa.nativeElement.placeholder = 'Digite a CRM do Médico';
        break;
      case 'especialidade':
      this.campopesquisa.nativeElement.placeholder = 'Digite a Especialidade do Médico';
      break;
      case 'nome':
      this.campopesquisa.nativeElement.placeholder = 'Digite o Nome do Médico';
      break;
      case 'datanascimento':
      this.showDateInput = true;
      break;
      default:
        break;
    }
   }
   Pesquisar() {
    this.pagina = 1;
    this.medicoservice.Pesquisar(this.ValorDaPesquisa(), this.parametropesquisa, this.pagina);
    }
    onScroll() {
      ++this.pagina;
      this.medicoservice.Pesquisar(this.ValorDaPesquisa(), this.parametropesquisa, this.pagina);
    }
    open() {
      const modalRef = this.modalService.open(MedicoCadastroComponent, {size: 'lg', windowClass: 'modal-adaptive'});
    }
    AbrirEditar(medico: Medico) {
      const modalRef = this.modalService.open(MedicoEditarComponent, {size: 'lg'});
      modalRef.componentInstance.medicoeditar = medico;
    }
    ValorDaPesquisa() {
      let val;
      if (this.showDateInput) {
        val = this.campoinputdatanascimento.nativeElement.value;
       } else {
        val = this.campopesquisa.nativeElement.value;
       }
       return val;
    }
}
