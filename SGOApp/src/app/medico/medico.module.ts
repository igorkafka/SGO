import { MedicoRoutingModule } from './medico-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MedicoComponent } from './medico/medico.component';
import { MedicoListaComponent } from './medico-lista/medico-lista.component';
import { NgbModalModule, NgbDatepickerModule, NgbTabsetModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { MedicoCadastroComponent } from './medico-cadastro/medico-cadastro.component';
import { MedicoEditarComponent } from './medico-editar/medico-editar.component';
import { MedicoListaService } from './medicolista.service';
// tslint:disable-next-line:import-spacing
import { MedicoPesquisaEspecialidadeComponent } from
'./medico-cadastro/medico-pesquisa-especialidade/medico-pesquisa-especialidade.component';

@NgModule({
  imports: [
    CommonModule,
    MedicoRoutingModule,
    NgbTabsetModule,
    InfiniteScrollModule,
    ReactiveFormsModule,
    NgbDatepickerModule,
    NgbModalModule
  ],
  declarations: [MedicoComponent, MedicoListaComponent, MedicoCadastroComponent, MedicoEditarComponent,
  MedicoPesquisaEspecialidadeComponent],
  providers: [MedicoListaService]
})
export class MedicoModule { }
