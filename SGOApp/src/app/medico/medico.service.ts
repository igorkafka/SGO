import { IPesquisa } from './../Interfaces/IPesquisa';
import { LoaderService } from './../Shared/loader.service';
import { MedicoListaService } from './medicolista.service';
import { Observable } from 'rxjs/observable';
import { AuthService } from './../core/auth.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Medico, ValoresParametrosMedico } from '../Models/medico.model';
@Injectable({
  providedIn: 'root'
})
export class MedicoService implements IPesquisa {
  constructor(private http: HttpClient, private auth: AuthService, private loader: LoaderService,
    private medicolistservice: MedicoListaService) {}
  Create(medico: Medico): Observable<void> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
   return this.http.post<void>('/api/medico/create', medico, {headers: headers});
  }
  Update(id: number, medico: Medico): Observable<void> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    return this.http.put<void>('/api/medico/atualizar?id=' + id, medico, {headers: headers});
  }
  Pesquisar(valor: any, parametro: string, pagina: number) {
    switch (parametro) {
      case ValoresParametrosMedico.nome :
       this.ListarPorNome(valor, pagina);
       break;
       case ValoresParametrosMedico.crm:
       this.ListarPorCrm(valor, pagina);
       break;
       case ValoresParametrosMedico.especialidade:
       this.ListarPorEspecialidade(valor, pagina);
       break;
       case ValoresParametrosMedico.datanascimento:
       this.ListarPorData(valor, pagina);
       break;
    }
  }
  ListarPorNome(nome: string, pagina: number) {
    this.loader.isLoading = true;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Medico[]>('/api/medico/listarpornome?nome=' + nome +
    '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      this.PreecherLista(data, pagina);
    }, error => {
      console.log(error);
      this.loader.isLoading = false;
    }, () => this.loader.isLoading = false);
  }
  ListarPorEspecialidade(especialidade: string, pagina: number) {
    this.loader.isLoading = true;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    this.http.get<Medico[]>('/api/medico/listarporespecialidade?especialidade='
     + especialidade + '&pagina=' + pagina, {headers: headers}).subscribe(data => {
      this.PreecherLista(data, pagina);
    }, error => {
      console.log(error);
      this.loader.isLoading = false;
    }, () => this.loader.isLoading = false);
  }
  ListarPorCrm(crm: string, pagina: number) {
    this.loader.isLoading = true;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    return this.http.get<Medico[]>('/api/medico/listarporcrm?crm=' + crm +
    '&pagina=' + pagina, {headers: headers}).subscribe( data => {
      this.PreecherLista(data, pagina);
    }, error => {
      console.log(error);
      this.loader.isLoading = false;
    }, () => this.loader.isLoading = false);
  }
  ListarPorData(datanascimento: string, pagina: number) {
    this.loader.isLoading = true;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    return this.http.get<Medico[]>('/api/medico/listarpordatanascimento?data='
     + datanascimento + '&pagina=' + pagina, {headers: headers}).subscribe( data => {
      this.PreecherLista(data, pagina);
    }, error => {
      console.log(error);
      this.loader.isLoading = false;
    }, () => this.loader.isLoading = false);
  }
  PreecherLista(ListaMedicos: Medico[], pagina: number) {
    if (pagina === 1) {
      this.medicolistservice.SetMedicos(ListaMedicos);
    } else {
      this.medicolistservice.Adicionar(ListaMedicos);
    }
  }
}
