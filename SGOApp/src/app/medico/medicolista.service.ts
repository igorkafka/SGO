import { IPesquisa } from './../Interfaces/IPesquisa';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Medico } from '../Models/medico.model';

@Injectable()
export class MedicoListaService {
  medicolistchanged: Subject<Medico[]> = new Subject();
  private medicos: Medico[] = new Array<Medico>();
  constructor() {}
  Adicionar(listaDeMedicos: Medico[]) {
    this.medicos.splice(this.medicos.length - 1, 0, ...listaDeMedicos);
    this.medicolistchanged.next(this.medicos);
  }
  AlterarLista(medico: Medico) {
    this.medicos.forEach((element, i) => {
      if (element.id === medico.id) {
        this.medicos[i] = medico;
      }
    });
  }
  SetMedicos(listaDeMedicos: Medico[]) {
    this.medicos = listaDeMedicos;
    this.medicolistchanged.next(this.medicos);
  }
}
