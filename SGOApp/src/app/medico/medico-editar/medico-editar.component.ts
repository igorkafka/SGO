import { Especialidade } from './../../Models/especialidade';
import { MedicoListaService } from './../medicolista.service';
import { LoaderService } from './../../Shared/loader.service';
import { MedicoService } from './../medico.service';
import { NgbActiveModal, NgbDatepickerConfig, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { Medico } from '../../Models/medico.model';

@Component({
  selector: 'app-medico-editar',
  templateUrl: './medico-editar.component.html',
  styleUrls: ['./medico-editar.component.css'],
})
export class MedicoEditarComponent implements OnInit {
  medicoeditarform: FormGroup;
  @ViewChild('t') tab: NgbTabset;
  @Input() medicoeditar: Medico;
  constructor(public activeModal: NgbActiveModal, private medicolistaservice: MedicoListaService,
     private medicoservice: MedicoService, private loaderservice: LoaderService,  config: NgbDatepickerConfig) {
      config.minDate = {year: 1900, month: 1, day: 1};
      config.maxDate = {year: 2099, month: 12, day: 31};
      console.log(this.medicoeditar);
  }
  ngOnInit() {
    this.initform();
  }
  initform() {
    const ValorDataNascimento = new Date(this.medicoeditar.dataNascimento);
    const DataNascimento = {
      day: ValorDataNascimento.getDate(),
      month: ValorDataNascimento.getMonth() + 1,
      year: ValorDataNascimento.getFullYear()
    };
    this.medicoeditarform = new FormGroup({
    'nome': new FormControl(this.medicoeditar.nome , [Validators.required]),
    'crm': new FormControl(this.medicoeditar.crm, [Validators.required]),
    'sexo': new FormControl(this.medicoeditar.sexo ? 'masculino' : 'feminino' ),
    'especialidadenome': new FormControl(this.medicoeditar.especialidade.nome, [Validators.required]),
    'especialidadeid': new FormControl(this.medicoeditar.especialidade.id, [Validators.required]),
    'datanascimento': new FormControl(DataNascimento, [Validators.required])
    }
    );
}
SetEspecialidade(especialidade: Especialidade) {
  this.tab.select('tab-editar');
  this.medicoeditarform.controls['especialidadenome'].setValue(especialidade.nome);
  this.medicoeditarform.controls['especialidadeid'].setValue(especialidade.id);
}
onSubmit(form) {
  if (form.valid) {
    const medico = new Medico();
    medico.id = this.medicoeditar.id;
    medico.crm = form.value.crm;
    medico.nome = form.value.nome;
    const especialidade = new Especialidade();
    especialidade.id = form.value.especialidadeid;
    especialidade.nome = form.value.especialidadenome;
    medico.especialidade = especialidade;
    medico.dataNascimento =
    new Date(form.value.datanascimento.year, form.value.datanascimento.month - 1,
    form.value.datanascimento.day);
    console.log(medico.dataNascimento);
    medico.sexo = form.value.sexo === 'masculino' ? true : false;
    this.loaderservice.isLoading = true;
    this.medicoservice.Update(this.medicoeditar.id, medico).subscribe(
      data =>  {
        this.loaderservice.isLoading = true;
        medico.id = this.medicoeditar.id;
        this.medicolistaservice.AlterarLista(medico);
      }, error => {
        this.loaderservice.isLoading = false;
        console.log(error);
      },
      () => {
        this.loaderservice.isLoading = false;
        this.activeModal.close();
      }
    );
} else {
    this.medicoeditarform.controls['nome'].markAsTouched();
    this.medicoeditarform.controls['crm'].markAsTouched();
    this.medicoeditarform.controls['especialidade'].markAsTouched();
    this.medicoeditarform.controls['datanascimento'].markAsTouched();
  }
}
}
