import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MedicoComponent } from './medico/medico.component';
import { AuthGuardAdminService } from '../core/AuthGuardAdminService';
import { MedicoListaComponent } from './medico-lista/medico-lista.component';
import { MedicoCadastroComponent } from './medico-cadastro/medico-cadastro.component';
import { MedicoEditarComponent } from './medico-editar/medico-editar.component';
@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'medico', component: MedicoComponent, canActivate: [AuthGuardAdminService], children: [
        {path: '', component: MedicoListaComponent},
        {path: 'cadastro', component: MedicoCadastroComponent},
        {path: 'editar', component: MedicoEditarComponent}
      ]}
    ])
  ],
  exports: [RouterModule]
})
export class MedicoRoutingModule {

}
