import { Especialidade } from './../../../Models/especialidade';
import { Component, OnInit, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { EspecialidadelistaService } from 'src/app/especialidade/especialidadelista.service';
import { EspecialidadeService } from 'src/app/especialidade/especialidade.service';

@Component({
  selector: 'app-medico-pesquisa-especialidade',
  templateUrl: './medico-pesquisa-especialidade.component.html',
  styleUrls: ['./medico-pesquisa-especialidade.component.css'],
  providers: [EspecialidadelistaService, EspecialidadeService]
})
export class MedicoPesquisaEspecialidadeComponent implements OnInit {
  especialidades: Especialidade[];
  @Output('selecionarEspecialidade') selecionarEspecialidade = new EventEmitter();
  @ViewChild('campoinput') campoinput: ElementRef;
  pagina: number = 1;
  constructor(private especialidadelistaservice: EspecialidadelistaService,
    private especialidadeservice: EspecialidadeService) { }

  ngOnInit() {
    this.especialidadelistaservice.especialidadelistachanged.subscribe(data => {
      this.especialidades = data;
    });
  }
  Pesquisar() {
    this.especialidadeservice.Pesquisar(this.campoinput.nativeElement.value, '', this.pagina);
  }
  onScroll() {
    this.pagina++;
    this.especialidadeservice.Pesquisar(this.campoinput.nativeElement.value, '', this.pagina);
  }
  SelecionarEspecialidade(especialidade: Especialidade) {
    this.selecionarEspecialidade.emit(especialidade);
  }
}
