import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicoPesquisaEspecialidadeComponent } from './medico-pesquisa-especialidade.component';

describe('MedicoPesquisaEspecialidadeComponent', () => {
  let component: MedicoPesquisaEspecialidadeComponent;
  let fixture: ComponentFixture<MedicoPesquisaEspecialidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicoPesquisaEspecialidadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicoPesquisaEspecialidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
