import { Medico } from './../../Models/medico.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, NgForm, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../../core/auth.service';
import { LoaderService } from '../../Shared/loader.service';
import { NgbActiveModal, NgbModal, NgbDatepickerConfig, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { MedicoService } from '../medico.service';
import { Especialidade } from 'src/app/Models/especialidade';

@Component({
  selector: 'app-medico-cadastro',
  templateUrl: './medico-cadastro.component.html',
  styleUrls: ['./medico-cadastro.component.css']
})
export class MedicoCadastroComponent implements OnInit {
  medicocadastroform: FormGroup;
  @ViewChild('t') tab: NgbTabset;
  constructor(private auth: AuthService, private medicoservice: MedicoService,   private loaderservice: LoaderService,
    public activeModal: NgbActiveModal, private modalService: NgbModal,
   config: NgbDatepickerConfig) {
    config.minDate = {year: 1900, month: 1, day: 1};
    config.maxDate = {year: 2099, month: 12, day: 31};
    this.initform();
   }
  ngOnInit() {
  }
  onSubmit(form) {
    if (form.valid) {
      const medico = new Medico();
      medico.crm = form.value.crm;
      medico.nome = form.value.nome;
      medico.especialidade = new Especialidade();
      medico.especialidade.nome = form.value.especialidadenome;
      medico.especialidade.id = form.value.especialidadeid;
      medico.dataNascimento =
      new   Date(form.value.datanascimento.year, form.value.datanascimento.month - 1,
      form.value.datanascimento.day);
      console.log(form.value.sexo);
      medico.sexo = form.value.sexo === 'masculino' ? true : false;
      this.loaderservice.isLoading = true;
      this.medicoservice.Create(medico).subscribe(
        data => {}, error => {
          console.log(error);
          this.loaderservice.isLoading = false;
        },
        () => {
          this.loaderservice.isLoading = false;
          this.activeModal.close();
        }
      );
  } else {
      this.medicocadastroform.controls['nome'].markAsTouched();
      this.medicocadastroform.controls['crm'].markAsTouched();
      this.medicocadastroform.controls['especialidade'].markAsTouched();
      this.medicocadastroform.controls['datanascimento'].markAsTouched();
      this.medicocadastroform.controls['especialidadenome'].markAsTouched();
    }
  }
  initform() {
    this.medicocadastroform = new FormGroup({
    'nome': new FormControl('', [Validators.required]),
    'crm': new FormControl('', [Validators.required]),
    'sexo': new FormControl('masculino'),
    'especialidadenome': new FormControl('', [Validators.required]),
    'especialidadeid': new FormControl('', [Validators.required]),
    'datanascimento': new FormControl('', [Validators.required])
    }
    );
  }
  SetEspecialidade(especialidade: Especialidade) {
    this.tab.select('tab-cadastro');
    this.medicocadastroform.controls['especialidadenome'].setValue(especialidade.nome);
    this.medicocadastroform.controls['especialidadeid'].setValue(especialidade.id);
  }
}
