import { ContatoComponent } from './contato/contato.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './core/login/login.component';
import { RegisterComponent } from './core/register/register.component';
import {HomeComponent} from './core/home/home.component';
@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: '', component: HomeComponent},
            { path: 'login', component: LoginComponent},
            { path: 'register', component: RegisterComponent},
            { path: 'contato', component: ContatoComponent}
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
