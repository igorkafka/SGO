import { Observable } from 'rxjs/observable';
import { Injectable } from '@angular/core';
import { RegisterUser } from 'src/app/Models/RegisterUser.model';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Login } from '../Models/login';
import { Token } from '../Models/Token';
import { User } from '../Models/User';
@Injectable()
export class AuthService {
  token: Token = null;
  Authenticated: boolean = false;
  constructor(private _httpclient: HttpClient) { }
  RegisterAccount(registeruser: RegisterUser): Observable<Token> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token.accessToken
    });
  return this._httpclient.post<Token>('/api/Login/register', registeruser, {headers: headers});
  }
  ListarUsuariosPorData(datanascimento: string, pagina: number): Observable<User[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token.accessToken
    });
    return this._httpclient.get<User[]>('/api/Login/listarpordata?datanascimento=' + datanascimento +
    '&pagina=' + pagina, {headers: headers});
  }
  ListarPorEndereco(endereco: string, pagina: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token.accessToken
    });
    return this._httpclient.get<User[]>('/api/Login/listarporendereco?endereco='
    + endereco + '&pagina=' + pagina, {headers: headers});
  }
  ListarPorEmail(email: string, pagina: number): Observable<User[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token.accessToken
    });
    return this._httpclient.get<User[]>('/api/Login/listarporemail?email=' + email +
    '&pagina=' + pagina, {headers: headers});
  }
  ListarPorMatricula(matricula: string, pagina: number): Observable<User[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token.accessToken
    });
    return this._httpclient.get<User[]>('/api/Login/listarpormatricula?matricula='
     + matricula + '&pagina=' + pagina, {headers: headers});
  }
  ListarUsuariosPorNome(nome: string, pagina: number): Observable<User[]> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token.accessToken
    });
    return this._httpclient.get<User[]>('/api/Login/listarpornome?nome=' + nome +
    '&pagina=' + pagina, {headers: headers});
  }
  LoginAccount(login: Login): Observable<Token> {
 return  this._httpclient.post<Token>('/api/login/login', login);
  }
  Update(id: string, user: User) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.token.accessToken
    });
    return this._httpclient.put<boolean>('/api/login/atualizar?id=' +
    id, user, {headers: headers});
  }
}
