import { AuthService } from './auth.service';
import { CanActivate, ActivatedRoute, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Injectable } from '@angular/core';
@Injectable()
export class AuthGuardFuncionarioService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.token.permissao === 'administrator') {
      return true;
    } else if (this.authService.token.permissao === 'funcionario') {
      return true;
    } else if (this.authService.token === null) {
      this.router.navigate(['/login']);
    }
 }
}
