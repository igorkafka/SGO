import { RegisterUser } from './../../Models/RegisterUser.model';
import { Component, OnInit } from '@angular/core';
import {  AuthService } from './../auth.service';
import { FormGroup, FormControl, Validators, AbstractControl, NgForm } from '@angular/forms';
import { RegexExpression } from '../../Shared/RegexExpression';
import { Token } from '../../Models/Token';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registeruserForm: FormGroup;
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.initform();
  }
  passwordConfirming(c: AbstractControl): any {
    if (!c.parent || !c) { return; }
    const pwd = c.parent.get('password');
    const cpwd = c.parent.get('confirmpassword');
    if (!pwd || !cpwd) { return ; }
    if (pwd.value !== cpwd.value) {
        return { invalid: true };
    }
}
  initform() {
    const registeruser = new RegisterUser();
    this.registeruserForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.pattern(RegexExpression.EmailRegex())]),
      'confirmpassword': new FormControl(registeruser.ConfirmPassword, [Validators.required, Validators.minLength(4),
         this.passwordConfirming] ),
      'password': new FormControl(registeruser.Password, [Validators.required])
    });
  }
  ngSubmit(form) {
    if (form.valid) {
    const registeruser = new RegisterUser();
    registeruser.userEmail = form.value.email;
    registeruser.userName = form.value.nome;
    registeruser.Password = form.value.password;
    registeruser.ConfirmPassword = form.value.confirmpassword;
    this.auth.RegisterAccount(registeruser).subscribe(
      data => {
        this.auth.token = new Token();
        this.auth.token = data;
      }
    );
    }
  }
}
