import { CanActivate, ActivatedRoute, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
@Injectable()
export class AuthGuardAdminService implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {
  }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.authService.token.permissao === 'administrator') {
      return this.authService.token.permissao === 'administrator' ? true : false;
    } else if (this.authService.token.permissao === 'funcionario') {
      return false;
    } else if (this.authService.token === null) {
      this.router.navigate(['/login']);
    }
 }
}
