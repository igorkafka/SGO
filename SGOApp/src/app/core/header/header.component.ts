import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../Shared/profile.service';
import { AuthService } from '../auth.service';
import { Token } from '../../Models/Token';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
 email: string = '';
  constructor(private profileservice: ProfileService, public auth: AuthService, private router: Router) { }
  ngOnInit() {
    this.email = this.profileservice.profile.email;
  }
  LogOut() {
    localStorage.clear();
    this.auth.token = null;
    this.auth.Authenticated = false;
    this.router.navigate(['/login']);
  }
}
