

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-rounting.module';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { AuthService } from './auth.service';
import { LoaderComponent } from './loader/loader.component';
import { NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { DocumentacaoComponent } from './documentacao/documentacao.component';


@NgModule({
  imports: [
    BrowserModule, CommonModule, AppRoutingModule, ReactiveFormsModule, NgbCarouselModule
  ],
  declarations: [HeaderComponent, HomeComponent, RegisterComponent, LoaderComponent, LoginComponent, DocumentacaoComponent],
  exports: [HeaderComponent, LoaderComponent, AppRoutingModule],
  providers: [AuthService]

})
export class CoreModule { }
