import { Router } from '@angular/router';
import {  AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { Login } from '../../Models/login';
import { RegexExpression } from '../../Shared/RegexExpression';
import { ProfileService } from '../../Shared/profile.service';
import { LoaderService } from '../../Shared/loader.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private auth: AuthService, private router: Router, private loaderService: LoaderService,
     private profileservice: ProfileService) {
     }

  ngOnInit() {
    this.initform();
  }
  initform() {
    const login = new Login();
    this.loginForm = new FormGroup({
      'email': new FormControl('', [Validators.required, Validators.pattern(RegexExpression.EmailRegex())]),
      'password': new FormControl('', [Validators.required])
    });
  }
  ngSubmit(form) {
    if (form.valid) {
      this.loaderService.isLoading = true;
      const login = new Login();
    login.UserEmail = form.value.email;
    login.Password = form.value.password;
    this.auth.LoginAccount(login).subscribe(data => {
      if (data.authenticated === true) {
        localStorage.setItem('id', data.id);
        localStorage.setItem('Authenticated', 'true');
        localStorage.setItem('accessToken', data.accessToken);
        localStorage.setItem('created', data.created);
        localStorage.setItem('expiration', data.expiration);
        localStorage.setItem('email',  login.UserEmail);
        localStorage.setItem('permissao', data.permissao);
        this.profileservice.profile.email = login.UserEmail;
        this.auth.token = data;
        this.auth.Authenticated = true;
        this.loaderService.isLoading = false;
        this.router.navigate(['/']);
      }  else {
        this.loaderService.isLoading = false;
      }
    }, error => {
      console.log(error);
      this.loaderService.isLoading = false;
    });
  }
}
}
