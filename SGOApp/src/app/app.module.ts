import { CausaModule } from './causa/causa.module';
import { EspecialidadeModule } from './especialidade/especialidade.module';
import { ContatoModule } from './contato/contato.module';
import {NgModule } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';
import { LOCALE_ID } from '@angular/core';
import { CoreModule } from './core/core.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-rounting.module';
import { HttpClientModule } from '@angular/common/http';
import { MedicoModule } from './medico/medico.module';
import { TaxaModule } from './taxa/taxa.module';
import { FuncionarioModule } from './funcionario/funcionario.module';
import { ClinicaModule } from './clinica/clinica.module';
import { PacienteModule } from './paciente/paciente.module';
import { BrowserModule } from '@angular/platform-browser';
import { AuthGuardFuncionarioService } from './core/authguardfuncionario.service';
import { AuthGuardAdminService } from './core/AuthGuardAdminService';
import { ObitoModule } from './obito/obito.module';
import { ContatoComponent } from './contato/contato.component';
registerLocaleData(localePt, 'pt-BR');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    ContatoModule,
    BrowserModule,
    AppRoutingModule,
    EspecialidadeModule,
    TaxaModule,
    HttpClientModule,
    PacienteModule,
    MedicoModule,
    FuncionarioModule,
    ClinicaModule,
    CoreModule,
    CausaModule,
    ObitoModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'pt-BR' }, AuthGuardAdminService, AuthGuardFuncionarioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
