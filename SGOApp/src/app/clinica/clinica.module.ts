import { ClinicaListaService } from './clinica-lista.service';
import { ClinicaService } from './clinica.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClinicaRoutingModule } from './clinica-routing.module';
import { ClinicaComponent } from './clinica.component';
import { ClinicaListaComponent } from './clinica-lista/clinica-lista.component';
import { ClinicaCadastroComponent } from './clinica-cadastro/clinica-cadastro.component';
import { ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { ClinicaEditarComponent } from './clinica-editar/clinica-editar.component';


@NgModule({
  imports: [
    CommonModule, ClinicaRoutingModule, ReactiveFormsModule, InfiniteScrollModule, NgbModalModule
  ],
  declarations: [ClinicaComponent, ClinicaListaComponent, ClinicaCadastroComponent, ClinicaEditarComponent],
  providers: [ClinicaService, ClinicaListaService]
})
export class ClinicaModule { }
