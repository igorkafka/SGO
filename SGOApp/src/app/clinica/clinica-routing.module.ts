import { AuthGuardAdminService } from './../core/AuthGuardAdminService';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ClinicaComponent } from './clinica.component';
import { ClinicaListaComponent } from './clinica-lista/clinica-lista.component';
import { ClinicaCadastroComponent } from './clinica-cadastro/clinica-cadastro.component';
import { ClinicaEditarComponent } from './clinica-editar/clinica-editar.component';
@NgModule({
    imports: [
        RouterModule.forChild([
            { path: 'clinica', component: ClinicaComponent, canActivate: [AuthGuardAdminService], children: [
                {path: '', component: ClinicaListaComponent},
                { path: 'cadastro', component: ClinicaCadastroComponent},
                {path: 'editar', component: ClinicaEditarComponent}
                ]
           }
        ])
    ],
    exports: [RouterModule]
})
export class ClinicaRoutingModule {

}
