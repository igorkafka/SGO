import { TestBed, inject } from '@angular/core/testing';

import { ClinicaListaService } from './clinica-lista.service';

describe('ClinicaListaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClinicaListaService]
    });
  });

  it('should be created', inject([ClinicaListaService], (service: ClinicaListaService) => {
    expect(service).toBeTruthy();
  }));
});
