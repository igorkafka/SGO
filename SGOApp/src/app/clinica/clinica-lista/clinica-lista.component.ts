
import { Clinica } from './../../Models/clinica';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ClinicaEditarComponent } from './../clinica-editar/clinica-editar.component';
import { ClinicaService } from './../clinica.service';
import { ClinicaCadastroComponent } from '../clinica-cadastro/clinica-cadastro.component';
import { ClinicaListaService } from '../clinica-lista.service';

@Component({
  selector: 'app-clinica-lista',
  templateUrl: './clinica-lista.component.html',
  styleUrls: ['./clinica-lista.component.css']
})
export class ClinicaListaComponent implements OnInit {
  clinicas: Clinica[];
  parametropesquisa: string = 'nome';
  subscription: Subscription;
  pagina: number = 1;
  @ViewChild('campoinput') campopesquisa: ElementRef;
  constructor(private modalService: NgbModal, private clinicaservice: ClinicaService,
    private clinicalistservice: ClinicaListaService) { }

  ngOnInit() {
    this.clinicalistservice.clinicalistachanged.subscribe(data => {
      this.clinicas = data;
    });
  }
  MudarParametroPesquisa(event) {
    this.parametropesquisa = event.target.value;
    this.pagina = 1;
    switch (this.parametropesquisa) {
      case 'nome':
      this.campopesquisa.nativeElement.placeholder = 'Digite o nome da clínica';
        break;
      case 'descricao':
      this.campopesquisa.nativeElement.placeholder = 'Digite a Descrição da clinica';
      break;
      default:
        break;
    }
   }
   Pesquisar() {
     this.pagina = 1;
     this.clinicaservice.Pesquisar(this.campopesquisa.nativeElement.value, this.parametropesquisa, this.pagina);
   }
   open() {
    const modalRef = this.modalService.open(ClinicaCadastroComponent, {size: 'lg', windowClass: 'modal-adaptive'});
   }
   onScroll() {
    this.pagina++;
    this.clinicaservice.Pesquisar(this.campopesquisa.nativeElement.value, this.parametropesquisa, this.pagina);
   }
   AbrirEditar(clinica: Clinica) {
    const modalRef = this.modalService.open(ClinicaEditarComponent, {size: 'lg', windowClass: 'modal-adaptive'});
    modalRef.componentInstance.clinicaeditar = clinica;
   }
}
