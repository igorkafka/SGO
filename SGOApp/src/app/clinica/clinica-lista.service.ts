import { Injectable } from '@angular/core';
import { Clinica } from '../Models/clinica';
import { Subject } from 'rxjs';
import { Medico } from '../Models/medico.model';

@Injectable({
  providedIn: 'root'
})
export class ClinicaListaService {
  clinicalistachanged: Subject<Clinica[]> = new Subject();
  clinicalista: Clinica[];
  constructor() { }
  Adicionar(listadeclinicas: Clinica[]) {
    this.clinicalista.splice(this.clinicalista.length - 1, 0, ...listadeclinicas);
    this.clinicalistachanged.next(this.clinicalista);
  }
  AlterarLista(clinica: Clinica) {
    this.clinicalista.forEach((element, i) => {
      if (element.id === clinica.id) {
        this.clinicalista[i] = clinica;
      }
    });
  }
  SetMedicos(listadeclinicas: Clinica[]) {
    this.clinicalista = listadeclinicas;
    this.clinicalistachanged.next(this.clinicalista);
  }
}
