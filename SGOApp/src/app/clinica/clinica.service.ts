import { IPesquisa } from './../Interfaces/IPesquisa';
import { LoaderService } from './../Shared/loader.service';
import { ClinicaListaService } from './clinica-lista.service';
import { AuthService } from './../core/auth.service';
import { Clinica, ValoresParametrosClinica } from './../Models/clinica';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClinicaService implements IPesquisa {

  constructor(private http: HttpClient, private clinicalist: ClinicaListaService,
     private auth: AuthService, private loader: LoaderService) {}
     Pesquisar(valor: any, parametro: string, pagina: number) {
      switch (parametro) {
        case ValoresParametrosClinica.nome :
         this.ListarPorNome(valor, pagina);
         break;
         case ValoresParametrosClinica.descricao:
         this.ListarPorDescricao(valor, pagina);
         break;
      }
    }
    ListarPorDescricao(descricao, pagina) {
      this.loader.isLoading = true;
      const headers = new HttpHeaders({
        'Authorization': 'Bearer ' + this.auth.token.accessToken
      });
       this.http.get<Clinica[]>('/api/clinica/listarpordescricao?descricao=' + descricao
      + '&pagina=' + pagina, {headers: headers}).subscribe(
         data => {
           this.PreencherLista(data, pagina);
         }, error => {
           this.loader.isLoading = false;
           console.log(error);
         }, () => {
           this.loader.isLoading = false;
         }
       );
    }
  Create(clinica: Clinica): Observable<void> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    return this.http.post<void>('/api/clinica/create', clinica, {headers: headers});
  }
  Update(id: number, clinica: Clinica): Observable<void> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    return this.http.put<void>('/api/clinica/update?id=' + id, clinica, {headers: headers});
  }
  ListarPorNome(nome: string, pagina: number) {
    this.loader.isLoading = true;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
     this.http.get<Clinica[]>('/api/clinica/listarpornome?nome=' + nome
    + '&pagina=' + pagina, {headers: headers}).subscribe(
       data => {
         this.PreencherLista(data, pagina);
       }, error => {
         console.log(error);
       }, () => {
         this.loader.isLoading = false;
       }
     );
  }
  PreencherLista(ListaClinica: Clinica[], pagina: number) {
    if (pagina === 1) {
      this.clinicalist.SetMedicos(ListaClinica);
    } else {
      this.clinicalist.Adicionar(ListaClinica);
    }
  }
}
