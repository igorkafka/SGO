import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { ClinicaService } from './../clinica.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LoaderService } from '../../Shared/loader.service';
import { Clinica } from '../../Models/clinica';

@Component({
  selector: 'app-clinica-cadastro',
  templateUrl: './clinica-cadastro.component.html',
  styleUrls: ['./clinica-cadastro.component.css'],
   encapsulation: ViewEncapsulation.None,
})
export class ClinicaCadastroComponent implements OnInit {
  clinicacadastroForm: FormGroup;
 @ViewChild('nomeenfermaria') nomeenfermaria: ElementRef;
 @ViewChild('descricaoenfermaria') descricaoenfermaria: ElementRef;
  constructor(public activeModal: NgbActiveModal, private clinicaservice: ClinicaService,
    private loader: LoaderService) { }
  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.clinicacadastroForm =  new FormGroup({
      'nome': new FormControl('', [Validators.required]),
      'descricao': new FormControl('', [Validators.required])
    });
  }
  onSubmit(form) {
    if (form.valid) {
      const clinica = new Clinica();
      clinica.nome = form.value.nome;
      clinica.descricao = form.value.descricao;
      this.loader.isLoading = true;
      this.clinicaservice.Create(clinica).subscribe(data => {
      }, error => {
        console.log(error);
        this.loader.isLoading = false;
      }, () => {
        this.loader.isLoading = false;
        this.activeModal.close();
      });
    } else {
      this.clinicacadastroForm.controls['nome'].markAsTouched();
      this.clinicacadastroForm.controls['descricao'].markAsTouched();
    }
  }
}
