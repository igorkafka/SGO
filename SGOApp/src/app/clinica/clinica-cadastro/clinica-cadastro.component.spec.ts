import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicaCadastroComponent } from './clinica-cadastro.component';

describe('ClinicaCadastroComponent', () => {
  let component: ClinicaCadastroComponent;
  let fixture: ComponentFixture<ClinicaCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicaCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicaCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
