import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClinicaEditarComponent } from './clinica-editar.component';

describe('ClinicaEditarComponent', () => {
  let component: ClinicaEditarComponent;
  let fixture: ComponentFixture<ClinicaEditarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClinicaEditarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClinicaEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
