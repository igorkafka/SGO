import { ClinicaService } from './../clinica.service';
import { LoaderService } from './../../Shared/loader.service';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { Component, OnInit, Input } from '@angular/core';
import { Clinica } from '../../Models/clinica';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ClinicaListaService } from '../clinica-lista.service';

@Component({
  selector: 'app-clinica-editar',
  templateUrl: './clinica-editar.component.html',
  styleUrls: ['./clinica-editar.component.css']
})
export class ClinicaEditarComponent implements OnInit {
  clinicaeditarform: FormGroup;
  @Input() clinicaeditar: Clinica;
  constructor(private loader: LoaderService, public activeModal: NgbActiveModal,
     private clinicaservice: ClinicaService, private clinicalistservice: ClinicaListaService) { }
  ngOnInit() {
    this.initForm();
  }
  initForm() {
    this.clinicaeditarform =  new FormGroup({
      'nome': new FormControl(this.clinicaeditar.nome, [Validators.required]),
      'descricao': new FormControl(this.clinicaeditar.descricao, [Validators.required]),
      'id': new FormControl(this.clinicaeditar.id)
    });
  }
  onSubmit(form) {
    if (form.valid) {
      const clinica = new Clinica();
      clinica.id = form.value.id;
      clinica.nome = form.value.nome;
      clinica.descricao = form.value.descricao;
      this.loader.isLoading = true;
      this.clinicaservice.Update(clinica.id, clinica).subscribe(data => {
        this.clinicalistservice.AlterarLista(clinica);
      }, error => {
        console.log(error);
        this.loader.isLoading = false;
      }, () => {
        this.loader.isLoading = false;
        this.activeModal.close();
      });
    } else {
      this.clinicaeditarform.controls['nome'].markAsTouched();
      this.clinicaeditarform.controls['descricao'].markAsTouched();
    }
  }
}
