import { EspecialidadeEditarComponent } from './../especialidade-editar/especialidade-editar.component';
import { EspecialidadeCadastroComponent } from './../especialidade-cadastro/especialidade-cadastro.component';
import { EspecialidadeService } from './../especialidade.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { EspecialidadelistaService } from '../especialidadelista.service';
import { Especialidade } from 'src/app/Models/especialidade';

@Component({
  selector: 'app-especialidade-lista',
  templateUrl: './especialidade-lista.component.html',
  styleUrls: ['./especialidade-lista.component.css']
})
export class EspecialidadeListaComponent implements OnInit {
  especialidades: Especialidade[];
  @ViewChild('campoinput') campopesquisa: ElementRef;
  pagina: number = 1;
  constructor(private especialidadeservice: EspecialidadeService,
    private especialidadelistaservice: EspecialidadelistaService, private modalService: NgbModal) { }

  ngOnInit() {
    this.especialidadelistaservice.especialidadelistachanged.subscribe(data => {
      console.log(data);
      this.especialidades = data;
    });
  }
  Pesquisar() {
    this.pagina = 1;
    this.especialidadeservice.Pesquisar(this.campopesquisa.nativeElement.value, '', this.pagina);
  }
  AbrirEditar(especialidade: Especialidade) {
    const modalRef = this.modalService.open(EspecialidadeEditarComponent, {size: 'sm', windowClass: 'modal-adaptive'});
    modalRef.componentInstance.especialidadeeditar = especialidade;
  }
  open() {
    const modalRef = this.modalService.open(EspecialidadeCadastroComponent, {size: 'sm', windowClass: 'modal-adaptive'});
  }
  onScroll() {
    this.pagina++;
    this.especialidadeservice.Pesquisar(this.campopesquisa.nativeElement.value, '', this.pagina);
  }
}
