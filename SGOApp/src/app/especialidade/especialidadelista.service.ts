import { Especialidade } from './../Models/especialidade';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EspecialidadelistaService {

  especialidadelistachanged: Subject<Especialidade[]> = new Subject();
  especialidadelista: Especialidade[];
  constructor() { }
  Adicionar(listadeespecialidades: Especialidade[]) {
    this.especialidadelista.splice(this.especialidadelista.length - 1, 0, ...listadeespecialidades);
    this.especialidadelistachanged.next(this.especialidadelista);
  }
  AlterarLista(especialidade: Especialidade) {
    this.especialidadelista.forEach((element, i) => {
      if (element.id === especialidade.id) {
        this.especialidadelista[i] = especialidade;
      }
    });
  }
  SetEspecialidades(listaadeespecialidades: Especialidade[]) {
    this.especialidadelista = listaadeespecialidades;
    this.especialidadelistachanged.next(this.especialidadelista);
  }
}
