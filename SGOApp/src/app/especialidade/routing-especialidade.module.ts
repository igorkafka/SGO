import { EspecialidadeEditarComponent } from './especialidade-editar/especialidade-editar.component';
import { AuthGuardAdminService } from './../core/AuthGuardAdminService';
import { EspecialidadeListaComponent } from './especialidade-lista/especialidade-lista.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EspecialidadeComponent } from './especialidade/especialidade.component';
import { EspecialidadeCadastroComponent } from './especialidade-cadastro/especialidade-cadastro.component';
@NgModule({
  imports: [
    RouterModule.forChild([
      { path: 'especialidade', canActivate: [AuthGuardAdminService], component: EspecialidadeComponent, children: [
        {path: '', component: EspecialidadeListaComponent},
        {path: 'cadastro', component: EspecialidadeCadastroComponent},
        {path: 'editar', component: EspecialidadeEditarComponent}
      ]}
    ])
  ],
  exports: [RouterModule]
})
export class EspecialidadeRoutingModule {

}
