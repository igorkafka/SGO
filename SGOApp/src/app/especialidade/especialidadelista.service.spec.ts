import { TestBed, inject } from '@angular/core/testing';

import { EspecialidadelistaService } from './especialidadelista.service';

describe('EspecialidadelistaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EspecialidadelistaService]
    });
  });

  it('should be created', inject([EspecialidadelistaService], (service: EspecialidadelistaService) => {
    expect(service).toBeTruthy();
  }));
});
