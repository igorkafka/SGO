import { EspecialidadeEditarComponent } from './especialidade-editar/especialidade-editar.component';
import { EspecialidadeRoutingModule } from './routing-especialidade.module';
import { EspecialidadeCadastroComponent } from './especialidade-cadastro/especialidade-cadastro.component';
import { EspecialidadeListaComponent } from './especialidade-lista/especialidade-lista.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EspecialidadeComponent } from './especialidade/especialidade.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
@NgModule({
  imports: [
    CommonModule,
    EspecialidadeRoutingModule,
    InfiniteScrollModule,
    NgbModalModule,
    ReactiveFormsModule,
  ],
  declarations: [EspecialidadeComponent, EspecialidadeListaComponent, EspecialidadeEditarComponent,
     EspecialidadeCadastroComponent]
})
export class EspecialidadeModule { }
