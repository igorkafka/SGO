import { Observable } from 'rxjs';
import { EspecialidadelistaService } from './especialidadelista.service';
import { Injectable } from '@angular/core';
import { IPesquisa } from './../Interfaces/IPesquisa';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../core/auth.service';
import { LoaderService } from '../Shared/loader.service';
import { MedicoListaService } from '../medico/medicolista.service';
import { Especialidade } from '../Models/especialidade';
@Injectable({
  providedIn: 'root'
})
export class EspecialidadeService implements IPesquisa {
  constructor(private http: HttpClient, private auth: AuthService, private loader: LoaderService,
   private especialidadelista: EspecialidadelistaService) {
  }
  Pesquisar(valor: any, parametro: string, pagina: number) {
    this.ListarPorNome(valor, pagina);
  }
  ListarPorNome(nome: string, pagina: number) {
    this.loader.isLoading = true;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
    return this.http.get<Especialidade[]>('/api/especialidade/listarpornome?nome='
    + nome + '&pagina=' + pagina, {headers: headers}).subscribe( data => {
      this.PreecherLista(data, pagina);
    }, error => {  console.log(error);
      this.loader.isLoading = false;
    }, () => this.loader.isLoading = false);
  }
  PreecherLista(ListaEspecialidades: Especialidade[], pagina: number) {
    if (pagina === 1) {
      this.especialidadelista.SetEspecialidades(ListaEspecialidades);
    } else {
      this.especialidadelista.Adicionar(ListaEspecialidades);
    }
  }
  Create(especialidade: Especialidade): Observable<void> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
   return this.http.post<void>('/api/especialidade/create', especialidade,
    {headers: headers});
  }
  Update(especialidade: Especialidade): Observable<void> {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + this.auth.token.accessToken
    });
   return this.http.put<void>('/api/especialidade/update', especialidade,
    {headers: headers});
  }
}
