import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/Shared/loader.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EspecialidadeService } from '../especialidade.service';
import { Especialidade } from 'src/app/Models/especialidade';

@Component({
  selector: 'app-especialidade-cadastro',
  templateUrl: './especialidade-cadastro.component.html',
  styleUrls: ['./especialidade-cadastro.component.css']
})
export class EspecialidadeCadastroComponent implements OnInit {
  especialidadecadastroform: FormGroup;
  constructor(private especialidadeservice: EspecialidadeService,   private loaderservice: LoaderService,
    public activeModal: NgbActiveModal) {
      this.initform();
    }

  ngOnInit() {
  }
  initform() {
    this.especialidadecadastroform = new FormGroup({
    'nome': new FormControl('', [Validators.required])
    }
    );
  }
  onSubmit(form) {
    if (form.valid) {
      const especialidade = new Especialidade();
      especialidade.nome = form.value.nome;
      this.loaderservice.isLoading = true;
      this.especialidadeservice.Create(especialidade).subscribe(data => {

      }, error => {
        console.log(error);
        this.loaderservice.isLoading = false;
      }, () => {
        this.loaderservice.isLoading = false;
        this.activeModal.close();
      });
    } else {
      this.especialidadecadastroform.controls['nome'].markAsTouched();
    }

  }

}
