import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EspecialidadeCadastroComponent } from './especialidade-cadastro.component';

describe('EspecialidadeCadastroComponent', () => {
  let component: EspecialidadeCadastroComponent;
  let fixture: ComponentFixture<EspecialidadeCadastroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EspecialidadeCadastroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EspecialidadeCadastroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
