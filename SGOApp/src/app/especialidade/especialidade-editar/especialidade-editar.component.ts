import { EspecialidadeService } from './../especialidade.service';
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { EspecialidadelistaService } from '../especialidadelista.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LoaderService } from 'src/app/Shared/loader.service';
import { Especialidade } from './../../Models/especialidade';
@Component({
  selector: 'app-especialidade-editar',
  templateUrl: './especialidade-editar.component.html',
  styleUrls: ['./especialidade-editar.component.css']
})
export class EspecialidadeEditarComponent implements OnInit {
  @Input() especialidadeeditar: Especialidade;
  especialidadeeditarform: FormGroup;
  constructor(public activeModal: NgbActiveModal, private especialidadelistservice: EspecialidadelistaService,
    private especialidadeservice: EspecialidadeService, private loaderservice: LoaderService) {
    }

  ngOnInit() {
    this.initform();
  }
  initform() {
    this.especialidadeeditarform = new FormGroup({
      'nome': new FormControl(this.especialidadeeditar.nome, Validators.required),
      'id': new FormControl(this.especialidadeeditar.id, [Validators.required])
    });
  }
  onSubmit(form) {
    if (form.valid) {
      const especialidade = new Especialidade();
      especialidade.nome = form.value.nome;
      especialidade.id = form.value.id;
      this.loaderservice.isLoading = true;
      this.especialidadeservice.Update(especialidade).subscribe(data => {
      }, error => {
        console.log(error);
      }, () => {
        this.especialidadelistservice.AlterarLista(especialidade);
        this.loaderservice.isLoading = false;
        this.activeModal.close();
      });
    } else {

    }
  }
}
